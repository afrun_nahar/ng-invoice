/**
 * Created by spider on 12/28/2017.
 */
angular.module('nproject')
    .controller('logOutCtrl', function ($scope, $state, authFactory) {
        $scope.logout = function () {
            localStorage.clear();
            $state.go("root.login",{})
        };
        $scope.isAuthorise = function(){
            return authFactory.isAuthenticated();
        };
    });


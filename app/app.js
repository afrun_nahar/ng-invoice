/**
 * Created by spider on 12/19/2017.
 */
angular.module("nproject",[
    'ngMessages',
    'ui.router',
    'dash.ui.state',
    //'angular.filter',
    'ngMaterial',
    'customFilter',
    'textAngular',
    'ngSanitize',
    'common',
])


.config(function($httpProvider, $mdDateLocaleProvider, $mdThemingProvider) {

    $httpProvider.interceptors.push('httpRequestInterceptor');

    $mdDateLocaleProvider.formatDate = function(date) {
        return moment(date).format('DD-MM-YYYY');
     };

    $mdThemingProvider.theme('default')
        .primaryPalette('blue')
        .accentPalette('red')

})

.run(function ($rootScope, $window, $state, authFactory) {
    $rootScope.title = 'DBCL';

    $rootScope.online = navigator.onLine;
    $window.addEventListener("offline", function(e) {
        $rootScope.$apply(function() {
            $rootScope.online = false;
        });
    }, false);

    $window.addEventListener("online", function(e) {
        $rootScope.$apply(function(e) {
            $rootScope.online = true;
        });
    }, false);

    $rootScope.$on("$stateChangeStart", function (event, toState, toParams, fromState, fromParams) {
        if (toState.authenticate) {
            if(authFactory.isAuthenticated()){
                if (!authFactory.isAuthrozied(toState.authorization)) {
                    $state.go('root.login');
                    event.preventDefault();  
                }
            } else {
                $state.go('root.login');
                event.preventDefault();
            }
        }
    });

})

.constant('config', {
    authUrl: 'http://192.168.10.124/api.app.ecure.oAuth/api/',
    apiUrl: 'http://192.168.10.124/api.ecure24.biopsy.core/api/',
    apiReceiptsUrl: 'http://192.168.10.124/api.ecure24.biopsy.receipt/api/',
    apiAccountsUrl: 'http://192.168.10.124/api.ecure24.biopsy.accounts/api/',
    apiPassUrl: 'http://192.168.10.124/app.ecure24.com/UserAuth/'
})

angular.module('common', []);
angular.module("commonDir", []);
angular.module("customFilter", []);
angular.module("dataresource", []);






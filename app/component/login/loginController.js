/**
 * Created by spider on 12/20/2017.
 */
angular.module('nproject')
    .controller('loginCtrl',function ($scope, $http, config, $state, tokenFactory, notifyFactory) {
        $scope.user = {
            //username:"administrator",
            //password:"1234",
            grant_type:"password"
        }

        //.factory('formEncode', function () {
        function formEncode(data) {
            var pairs = [];
            for (var name in data) {
                pairs.push(encodeURIComponent(name) + '=' + encodeURIComponent(data[name]));
            }
            return pairs.join('&').replace(/%20/g, '+');
        }
        //});

        $scope.login = function () {
            $scope.disableBtn = true;

            var _data = formEncode($scope.user);
            $http.post(config.authUrl + 'ecure/token', _data)
                .then(function (response) {
                    //$scope.disableBtn = false;
                    //localStorage.setItem('user-token', JSON.stringify(response.data));
                    tokenFactory.accessToken($scope.user.username, response.data.access_token)
                    notifyFactory.success('login successful');

                        $state.go("root.dashboard.home", {});

                }, function(response){
                    $scope.disableBtn = false;
                    //$scope.errorMessage = response.data.error;
                    if (response.data.error) {
                        $scope.errorMessage = "Invalid Username or Password";
                    }
                    
                })
        };



    });
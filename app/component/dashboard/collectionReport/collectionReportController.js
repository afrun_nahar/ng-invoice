/**
 * Created by spider on 1/9/2018.
 */
angular.module('nproject')
    .controller('collectionReportCtrl',['$scope', '$http', 'config', '$filter', 'resourceCollecectionReport', 
    function ($scope, $http, config, $filter, resourceCollecectionReport) {
        $scope.src = searchReset();
        // $scope.src = {
        //     "fromDate": new Date("2018-01-10T11:15:43.830Z"),
        //     "toDate": new Date("2018-01-10T11:15:43.830Z"),
        //     "doctorId": "636506798895143194",
        //     "referralId": "636511758068880610",
        //     "userId": "U-10007"
        // }
        $scope.Reset = function(){
            $scope.src = searchReset();
            $scope.ref.searchText = "";
            $scope.doc.searchText = "";
			$scope.mkt.searchText = "";
			
        };

        function searchReset(){
            return {
                fromDate: new Date(),
                toDate: new Date()
            };
        }

        // resourceCompanyProfile.get().then(function(res){
        //     $scope.profile = res.data;
        // })

        $scope.Search = function(){
            $scope.ReportFor = 'date';
            // var fromDate = convertToDate(src.fromDate);
            // var toDate = convertToDate(src.toDate);

            //var stringPath = "fromDate/"+fromDate+"/toDate/"+toDate+"/pageNumber/1/pageSize/1000";
            // if ($scope.src.doctorId) {
            //     //stringPath = "doctor/"+ $scope.src.doctorId + "/" + stringPath;
            //     return resourceCollecectionReport.GetDateRange(fromDate, toDate, 1, 1000)
            //         //.then(SetDataToModel)
            // } else {
            //     if ($scope.src.referralId) {
            //         //stringPath = "referral/"+ $scope.src.referralId + "/" + stringPath;
            //         return resourceCollecectionReport.GetDateRange(fromDate, toDate, 1, 1000)
            //             //.then(SetDataToModel)
            //     } else {
            //         if ($scope.src.userId) {
            //             //stringPath = "user/"+ $scope.src.userId + "/" + stringPath;
            //             return resourceCollecectionReport.GetDateRange(fromDate, toDate, $scope.src.userId, 1, 1000)
            //             //.then(SetDataToModel)
            //         }
            //     }
            // }
            
            


            //$http.get(config.apiAccountsUrl + "cashCollections/" + stringPath)
            SearchCondition($scope.src)
                .then(SetDataToModel)

            
        }

        function SearchCondition(src){
            var fromDate = convertToYMD(src.fromDate);
            var toDate = convertToYMD(src.toDate);
            
			if(src.referralMarketingId){
					$scope.ReportFor = 'Marketing';
					return resourceCollecectionReport.GetByMarketingReferral(src.referralMarketingId, fromDate, toDate, 1, 1000);
				}
				
            if (src.doctorId) {
                //stringPath = "doctor/"+ $scope.src.doctorId + "/" + stringPath;
                // $scope.ReportFor = 'doctor';
                // return resourceCollecectionReport.GetByDocotr(fromDate, toDate, src.doctorId, 1, 1000);
                if (src.referralId) {
                    $scope.ReportFor = 'DocotrAndHospital';
                    return resourceCollecectionReport.GetByReferralAndDoctor(src.referralId, src.doctorId, fromDate, toDate, 1, 1000);
                } 
				else {
                    $scope.ReportFor = 'doctor';
                    return resourceCollecectionReport.GetByDocotr(fromDate, toDate, src.doctorId, 1, 1000);
                }
            } else {
                if (src.referralId) {
                    $scope.ReportFor = 'hospital';
                    return resourceCollecectionReport.GetByHospital(fromDate, toDate, src.referralId, 1, 1000);
                } else {
                    if (src.employeeId) {
                        $scope.ReportFor = 'employeeId';
                        return resourceCollecectionReport.GetByUser(fromDate, toDate, src.employeeId, 1, 1000);
                    } else {
                        return resourceCollecectionReport.GetDateRange(fromDate, toDate, 1, 1000);
                    }
                }
            }
        }

        function SetDataToModel(result){
            $scope.ReportList = $filter('filter')(result.data, { collectionType: '!Bundle Due Collection' } )
            //$scope.ReportList = result.data;
        }

        function convertToYMD(date){
            return $filter('date')(date, 'yyyy-MM-dd')
        }

        $http.get(config.authUrl + "User/Get")
            .then(function (result) {
                $scope.userList = result.data;
            })
        $scope.querySearch = function (method, _txtSrc) {
            return $http.get(config.apiUrl + "autocompletes/" + method + "/" + _txtSrc)
                .then(function (result) {
                    return result.data;
                })
        };
        $scope.changeAutocomplete = function(model, item){
            if (item) {
                $scope.src[model] = item.id
            } else {
                $scope.src[model] = item;
            }
        }
        

    }]);
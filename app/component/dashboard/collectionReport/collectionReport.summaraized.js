/**
 * Created by spider on 1/9/2018.
 */
angular.module('nproject')
    .controller('collectionReportSummaraizedCtrl',['$scope', '$http', 'config', '$filter', 'resourceCollecectionReport', 'resourceCompanyProfile', 
    function ($scope, $http, config, $filter, resourceCollecectionReport, resourceCompanyProfile) {
        $scope.src = searchReset();
        
        $scope.Reset = function(){
            $scope.src = searchReset();
            $scope.ref.searchText = "";
            $scope.doc.searchText = "";
        };

        function searchReset(){
            return {
                fromDate: new Date(),
                toDate: new Date()
            };
        }

        resourceCompanyProfile.get().then(function(res){
            $scope.profile = res.data;
        })

        $scope.Search = function(){
            $scope.ReportFor = 'date';
            // var fromDate = convertToDate(src.fromDate);
            // var toDate = convertToDate(src.toDate);

            //var stringPath = "fromDate/"+fromDate+"/toDate/"+toDate+"/pageNumber/1/pageSize/1000";
            // if ($scope.src.doctorId) {
            //     //stringPath = "doctor/"+ $scope.src.doctorId + "/" + stringPath;
            //     return resourceCollecectionReport.GetDateRange(fromDate, toDate, 1, 1000)
            //         //.then(SetDataToModel)
            // } else {
            //     if ($scope.src.referralId) {
            //         //stringPath = "referral/"+ $scope.src.referralId + "/" + stringPath;
            //         return resourceCollecectionReport.GetDateRange(fromDate, toDate, 1, 1000)
            //             //.then(SetDataToModel)
            //     } else {
            //         if ($scope.src.userId) {
            //             //stringPath = "user/"+ $scope.src.userId + "/" + stringPath;
            //             return resourceCollecectionReport.GetDateRange(fromDate, toDate, $scope.src.userId, 1, 1000)
            //             //.then(SetDataToModel)
            //         }
            //     }
            // }
            
            


            //$http.get(config.apiAccountsUrl + "cashCollections/" + stringPath)
            SearchCondition($scope.src)
                .then(SetDataToModel)

            
        }

        function SearchCondition(src){
            var fromDate = convertToYMD(src.fromDate);
            var toDate = convertToYMD(src.toDate);
            
            if (src.doctorId) {
                //stringPath = "doctor/"+ $scope.src.doctorId + "/" + stringPath;
                // if (src.referralId) {
                //     return resourceCollecectionReport.GetByReferralAndDoctor(src.referralId, src.doctorId, fromDate, toDate, 1, 1000);
                // } else {
                    $scope.ReportFor = 'docotr';
                    return resourceCollecectionReport.GetByDocotr(fromDate, toDate, src.doctorId, 1, 1000);
                // }
            } else {
                if (src.referralId) {
                    $scope.ReportFor = 'hospital';
                    return resourceCollecectionReport.GetByHospital(fromDate, toDate, src.referralId, 1, 1000);
                } else {
                    if (src.userId) {
                        $scope.ReportFor = 'user';
                        return resourceCollecectionReport.GetByUser(fromDate, toDate, src.userId, 1, 1000);
                    } else {
                        return resourceCollecectionReport.GetDateRange(fromDate, toDate, 1, 1000);
                    }
                }
            }
        }

        function SetDataToModel(result){
            var data = result.data;
            //var data = groupBy(result.data, 'collectionType');
            //var data = $filter('orderBy')(result.data, 'collectionType');
            $scope.ReportList = data // groupBy(result.data, 'collectionType');
            //$scope
        }

        $scope.rowModify = function(currentValu, previousVal){
            //$filter('filter')(result.data, 'collectionType');
            var CalculateTotal = false;
            if (previousVal) {
                if (currentValu != previousVal) {
                    console.log('yes new')
                    CalculateTotal = true;
                }
            }

            return {
                calculate: CalculateTotal
            }
            
        }

        

        function convertToYMD(date){
            return $filter('date')(date, 'yyyy-MM-dd')
        }

        $http.get(config.authUrl + "User/Get")
            .then(function (result) {
                $scope.userList = result.data;
            })
        $scope.querySearch = function (method, _txtSrc) {
            return $http.get(config.apiUrl + "autocompletes/" + method + "/" + _txtSrc)
                .then(function (result) {
                    return result.data;
                })
        };
        $scope.changeAutocomplete = function(model, item){
            if (item) {
                $scope.src[model] = item.id
            } else {
                $scope.src[model] = item;
            }
            
        }

        function groupBy(xs, key) {
            return xs.reduce(function(rv, x) {
              (rv[x[key]] = rv[x[key]] || []).push(x);
              return rv;
            }, {});
          };
        

    }]);
/**
 * Created by spider on 3/6/2018.
 */
(function(){
    angular.module('nproject')
        .controller('monthReportCtrl', monthwiseDue);

    function monthwiseDue($scope, $http, config, $filter, customDate) {

        $scope.src = {
            fromDate :  new Date()
            , toDate : new Date(),
            referralId:''
        };

        $scope.changeReferral=function (referral) {
            $scope.src.referralId=referral.id;
        };

        $scope.Search = function(){

            // var startDate = $filter('date')($scope.src.fromDate, 'yyyy-MM-dd');
            // var endDate = $filter('date')($scope.src.toDate, 'yyyy-MM-dd');
            //
            // var url='specimenReceipts/fromDate/'+startDate+'/toDate/'+endDate;

            var startDate = $filter('date')($scope.src.fromDate, 'yyyy-MM-dd')
            var endDate = $filter('date')($scope.src.toDate, 'yyyy-MM-dd')
            var url='dueReceipts/fromDate/'+startDate+'/toDate/'+endDate;

            if ($scope.src.referralId)
              url='dueReceipts/fromDate/'+startDate+'/toDate/'+endDate+'/referral/' +$scope.src.referralId;
            $http.get(config.apiReceiptsUrl + url)
                .then(function (response) {
                    $scope.monthwiseDuesList = response.data;
                });
        };

        // $scope.calcTotal = function(){
        //     var total = 0;
        //     angular.forEach($scope.item, function(item){
        //         total = (item.January + item.february).toFixed(2);
        //     })
        //     return total;
        // }

        $scope.columns = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ]
        
        $scope.totalSum = function(item) {
            var items = $scope.columns;
            var rowTotal = 0;
            for (let i = 0; i < items.length; i++) {
                const elem = items[i];
                if (item[elem]) {
                    rowTotal += parseInt(item[elem]);
                }
                
            }
            return rowTotal;
            //console.log(item)
            //return parseInt(item.January) + parseInt(item.February) + parseInt(item.March);
        };

        $scope.GrendTotal = function (items) {
            var grandTotal = 0;
            if (items) {
                for (let i = 0; i < items.length; i++) {
                    const element = items[i];
                    grandTotal += $scope.totalSum(element);
                }
            }
            return grandTotal;
        }

        $scope.querySearch = function(method, _txtSrc){
            return $http.get(config.apiUrl + "autocompletes/"+ method + "/" + _txtSrc)
                .then(function(result){
                    return result.data;
                })
        }


    }
})()

/**
 * Created by spider on 2/4/2018.
 */
angular.module('nproject')
    .controller('balanceSheetCtrl', function ($scope, balanceSheet) {

        balanceSheet.GetAsset()
            .then(function (response) {
                $scope.main = response.data;
            });
         balanceSheet.GetLiability()
            .then(function (response) {
                $scope.liability = response.data;
            });

    });
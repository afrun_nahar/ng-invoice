/**
 * Created by spider on 1/11/2018.
 */
angular.module('nproject')
    .controller('trailBalanceCtrl',['$scope', '$http','config','$stateParams', function ($scope, $http, config,$stateParams) {

        $scope.search = function () {
            $http.get(config.apiAccountsUrl + 'trialBalances/fiscalYear/FY2017-18')
                .then(function(response){
                    $scope.balanceList = response.data;
                })
        }

    }]);
angular.module('nproject')
    .controller('voucherPrintCtrl', function($scope, $stateParams, $http, config) {
        
        $scope.stateParams = $stateParams;

        $http.get(config.apiAccountsUrl + "journals/" + $stateParams.id)
            .then(function(response){
                $scope.voucher = response.data;
            })
        
    });
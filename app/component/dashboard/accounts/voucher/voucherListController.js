/**
 * Created by spider on 1/13/2018.
 */
angular.module('nproject')
    .controller('voucherListCtrl',
     function ($scope, $http, $state, $stateParams, $filter, ListFactory, paginationFactory, JournalService) {
        $scope.src = {
            fromDate: setDate($stateParams.fromDate) ,
             toDate: setDate($stateParams.toDate)
        }
        var searchString = "fromDate/"+toDate($scope.src.fromDate)+"/toDate/" + toDate($scope.src.toDate)
        var lf = new ListFactory($stateParams.pageNumber, $stateParams.pageSize, searchString);
        LoadData();
        function LoadData(){
            JournalService.Get(lf.paramsStr, lf.paging)
                .then(function(response){
                    $scope.journals = response.data
                    var data = JSON.parse(response.pagination);
                    lf.paging.total = Math.ceil(data.TotalCount / lf.paging.pageSize);
                    $scope.buttonList = paginationFactory.getItems(lf.paging.total, lf.paging.current);
                });
        }
        
        function toDate(_date){
            return $filter('date')(_date, 'yyyy-MM-dd');
        }

        function setDate(date){
            if(date){
                return new Date(date);
            } else {
                return new Date();
            }
        }

        $scope.toDate = function(date){
            return toDate(date)
        }

        $scope.go = function(_pageNumber){
            $state.go("root.dashboard.accounts.voucherList", { fromDate: toDate($scope.src.fromDate), toDate: toDate($scope.src.toDate), pageNumber: _pageNumber, pageSize: $scope.paging.pageSize})
        }

        $scope.querysearch = function(_txtSrc){
            $http.get(config.apiAccountsUrl + "method/" + _txtSrc)
                .then(function (result) {
                    return result.data;
                });
        }

        // $scope.changeAutocompleteValue = function(item) {
            
        // }


        angular.extend($scope, lf);


    })



.factory('ListFactory', function(JournalService) {
        var ListFactory = function (pageNumber, pageSize, paramsStr) {
            var vm = this;
            //vm.primaryId = primaryId;
            vm.paging = {
                total: 0,
                current: pageNumber,
                pageSize: pageSize
            };
            vm.paramsStr = paramsStr;
            vm.buttonList = [];
    
            //vm.loadPages = function() {

                //return JournalService.Get(vm.paramsStr, vm.paging)

                //console.log('Current page is : ' + $scope.paging.current);
                // var pageingString = "pageNumber/"+vm.paging.current+"/pageSize/" + vm.paging.pageSize;
                
                // //if (vm.primaryId) {
                //     $http.get(config.apiUrl + "journals/" + vm.paramsStr + "/" + pageingString)
                //         //$http.get(config.apiUrl + "jobs/" + paramStr)
                //         .then(function (response) {
                //             //$scope.jobsItems = response.data;
                //             return response.headers('x-pagination');
                //         })
                //         .then(function (res) {
                //             var data = JSON.parse(res);
                //             vm.paging.total = Math.ceil(data.TotalCount / $stateParams.pageSize);
                //             //$scope.buttonList = paginationFactory.getItems(vm.paging.total, vm.paging.current);
                //         });
                //}
                
            //}
            
            return vm;
        };
        return ListFactory;
    
})

.service("JournalService", function ($http, config) {
    this.api = config.apiAccountsUrl;
    this.Get = function (paramsStr, paging) {
        var pageingString = "pageNumber/"+paging.current+"/pageSize/" + paging.pageSize;
        
        //if (vm.primaryId) {
            return $http.get(this.api + "journals/" + paramsStr + "/" + pageingString)
                //$http.get(config.apiUrl + "jobs/" + paramStr)
                .then(function (response) {
                    //$scope.jobsItems = response.data;
                    return { data: response.data, pagination: response.headers('x-pagination') };
                })
    }
})

// angular.module('nproject')
//     .factory('ListFactory',['$http', function($http){
//         function ListFactory(pageNumber, pageSize, paramsStr){
//             var vm = this;
//             //vm.primaryId = primaryId;
//             vm.paging = {
//                 total: 0,
//                 current: pageNumber,
//                 pageSize: pageSize
//             };
//             vm.paramsStr = paramsStr;
    
//             vm.loadPages = function() {
//                 //console.log('Current page is : ' + $scope.paging.current);
//                 var pageingString = "pageNumber/"+vm.paging.current+"/pageSize/" + vm.paging.pageSize;
                
//                 //if (vm.primaryId) {
//                     $http.get(config.apiUrl + "journals/" + vm.paramsStr + "/" + pageingString)
//                         //$http.get(config.apiUrl + "jobs/" + paramStr)
//                         .then(function (response) {
//                             //$scope.jobsItems = response.data;
//                             return response.headers('x-pagination');
//                         })
//                         .then(function (res) {
//                             var data = JSON.parse(res);
//                             vm.paging.total = Math.ceil(data.TotalCount / $stateParams.pageSize);
//                             //$scope.buttonList = paginationFactory.getItems(vm.paging.total, vm.paging.current);
//                         });
//                 //}
                
//             }
//             vm.loadPages()
            
//             return vm;
//         }
//         return this;
//     }]);

    
    
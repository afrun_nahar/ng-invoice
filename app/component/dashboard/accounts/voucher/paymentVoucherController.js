/**
 * Created by spider on 1/13/2018.
 */
(function(){
angular.module('nproject')
    .controller('paymentVoucherCtrl', function($scope, $http, $mdDialog, voucherFactory, config, $stateParams, $state, notifyFactory, $filter, $window, $mdToast) {

        var vm = voucherFactory.VoucherController();
        vm.Voucher = voucherFactory.PaymentVoucher('PV');
        vm.primaryId = $stateParams.id || "";
        vm.Save = function () {
            var DrTotal = $filter('calculateBy')(vm.Voucher.voucherSub.dr, 'dr');
            vm.Voucher.voucherSub.cr[0].cr = DrTotal; //voucherFactory.Journal('cr', '', DrTotal);
            var _voucher = new voucherFactory.Voucher('PV');
            _voucher.voucherSub = getVoucherSub(vm.Voucher.voucherSub.dr, vm.Voucher.voucherSub.cr);
            
            var opts = {
                method: $stateParams.id ? "PUT" : "POST",
                url: config.apiAccountsUrl + "journals" + ($stateParams.id ? "/" + $stateParams.id : ""),
                data: _voucher
            };
            
            $http(opts)
                .then(function (response) {
                    notifyFactory.success("Successfully " + (this.primaryId ? "updated" : "saved"))

                    setTimeout(function(){
                        vm.reload();
                    }, 1000);

                    // goprintandlist("20180100014", function(){
                    //     $state.go('root.dashboard.labInvoice', { });
                    // });
                    
                }, function () {
                    //$scope.buttonDisable = false;
                });
            
        }

        function getVoucherSub(DrArray, CrArray){
            var vs = [];
            for (var i = 0; i < DrArray.length; i++) {
                var dr = DrArray[i];
                vs.push(dr);
            }
            for (var i = 0; i < CrArray.length; i++) {
                var cr = CrArray[i];
                vs.push(cr);
            }
            return vs;
        }

        

        angular.extend($scope, vm);

        
    });

})();
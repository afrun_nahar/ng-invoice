/**
 * Created by spider on 12/27/2017.
 */
angular.module('nproject')
    .controller('journalVoucherCtrl', function($scope, $http, $mdDialog, voucherFactory, config, $stateParams) {
        
        var newScope = voucherFactory.VoucherController();
        newScope.primaryId = $stateParams.id;
        angular.extend($scope, newScope);
        
    });
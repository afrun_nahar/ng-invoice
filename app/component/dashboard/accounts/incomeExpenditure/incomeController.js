/**
 * Created by spider on 12/27/2017.
 */
angular.module('nproject')
    .controller('incomeCtrl', ['$scope', '$http', 'config', '$filter',
        function ($scope, $http, config, $filter) {
            $scope.src = searchReset();

            function searchReset(){
                return {
                    fromDate: new Date(),
                    toDate: new Date()
                };
            }

            $scope.Search = function(){
                $scope.ReportFor = 'date';
                var fDate = $filter('date')($scope.src.fromDate, 'yyyy-MM-dd');
                var tDate = $filter('date')($scope.src.toDate, 'yyyy-MM-dd');


                //$http.get(config.apiAccountsUrl + '/incomeStatements/fromdate/2018-01-01/todate/2018-12-12/')
                                                      //incomeStatements/fromdate/2018-01-28/todate/2018-01-28
                $http.get(config.apiAccountsUrl + '/incomeStatements/fromdate/'+fDate+'/todate/'+tDate)
                    .then(function (response) {
                        $scope.main = response.data;
                    });
            };
            $scope.Search();


            $scope.getTotal = function () {

                var incomeArray = $filter('filter')($scope.main,{category: 'I'});
                var expanseArray = $filter('filter')($scope.main,{category: 'E'});

                var income = $filter('calculateBy')(incomeArray,'amount');
                var expanse = $filter('calculateBy')(expanseArray,'amount');

                return Math.abs(income) - expanse;
            };

            $scope.getAbsTotal = function (total) {
                return Math.abs(total);
            }

        }]);
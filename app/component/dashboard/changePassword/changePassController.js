/**
 * Created by spider on 1/28/2018.
 */
angular.module('nproject')
    .controller('changePassCtrl',['$scope', '$http','config','$mdToast','$state', function ($scope, $http, config, $mdToast,$state) {
        $scope.enableBtn = true;
        $scope.changePassword =function () {
            $scope.enableBtn = false;

            $http.post(config.authUrl + 'Password/Change',$scope.main)
                .then(function (response) {
                    $scope.enableBtn = true;
                    $scope.user = response.data;

                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('successfully saved')
                            .position('top right')
                            .hideDelay(3000)
                    );

                    return response.data;

                });
            //$state.go("root.dashboard.home", {}, { reload: true });
                }




    }]);

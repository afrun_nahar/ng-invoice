/**
 * Created by spider on 1/7/2018.
 */
angular.module('nproject')
    .controller('createTmpltCtrl',['$scope', '$http', 'config','$mdToast','$stateParams','$state', function ($scope, $http, config, $mdToast, $stateParams,$state) {

        /*$scope.showDialog = function(ev) {
         $mdDialog.show({
         controller: invoiceDialogCtrl,
         //controllerAs: 'dLab',
         templateUrl: 'component/dashboard/lab/invoiceTemplate/dialogTemplate.html',
         parent: angular.element(document.body),
         targetEvent: ev,
         clickOutsideToClose:true
         })
         };*/

        // function invoiceDialogCtrl($scope, $mdDialog) {
        //
        //     $scope.key = {}
        //
        //     $scope.cancel = function () {
        //         $mdDialog.cancel();
        //     };
        //
        //     $scope.answer = function (answer) {
        //         $mdDialog.hide(answer);
        //     };
        $scope.key = {};


        $scope.primaryId = $stateParams.id;

        if ($scope.primaryId) {
            $http.get(config.apiUrl + 'specimenTemplates/' + $scope.primaryId)
                .then(function(response){
                    $scope.key = response.data;
                    $scope.selectedItem = response.data.specimenInfo;
                })
                // .then(function(){
                //     $scope.selectedItem = {}
                // })
            //$scope.getTemplateById($stateParams.id);
        }


        $scope.enableBtn = true;
        $scope.save =function () {
            $scope.enableBtn = false;


            if($scope.key){
                if ($stateParams.id) {
                    $http.put(config.apiUrl + "specimenTemplates/" + $scope.key.id , $scope.key)
                        .then(function (response) {
                            $scope.data = response.data;
                            $mdToast.show(
                                $mdToast.simple()
                                    .textContent('successfully saved')
                                    .position('top right')
                                    .hideDelay(3000)
                            );

                            return response.data;
                        });
                } else  {
                    $http.post(config.apiUrl + "specimenTemplates", $scope.key)
                        .then(function (response) {
                            $scope.data = response.data;
                            $mdToast.show(
                                $mdToast.simple()
                                    .textContent('successfully saved')
                                    .position('top right')
                                    .hideDelay(3000)
                            );

                            return response.data;
                        }, function (response) {
                            $mdToast.show(
                                $mdToast.simple()
                                    .textContent(response.data)
                                    .position('top right')
                                    .hideDelay(3000)
                            );
                        });
                }
                //$state.go("root.dashboard.invoiceTemplate", {});

            }
        }

        /*$scope.save = function () {
            $scope.buttonDisable = true;
            if ($scope.key) {
                $http.post(config.apiUrl + "specimenTemplates", $scope.key)
                    .then(function (response) {
                        $scope.data = response.data;
                        $scope.cancel();
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('successfully saved')
                                .position('top right')
                                .hideDelay(3000)
                        );
                        //$scope.getlist()
                        return response.data;
                    });
            }
        }*/
        $scope.querySearch = function(_txtSrc){
            return $http.get(config.apiUrl + 'autocompletes/specimen/' + _txtSrc)
                .then(function(result){
                    return result.data;
                })
        }

        $scope.GetTemplateById = function(_id){
            if (_id) {
                $scope.key = {};
                $http.get(config.apiUrl + 'specimenTemplates/specimen/' + _id)
                    .then(function(response){
                        if(response.data){
                            $scope.key= response.data;
                        } else {
                            $scope.key.specimen_InfoId = _id;
                        }
                    });
            }
            
        }



    }]);
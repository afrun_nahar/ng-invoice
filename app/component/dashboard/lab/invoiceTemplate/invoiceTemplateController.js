/**
 * Created by spider on 12/30/2017.
 */
angular.module('nproject')
    .controller('invoiceTemplateCtrl',['$scope', '$http', 'config', '$mdDialog','$mdToast', function ($scope, $http, config, $mdDialog,$mdToast) {

        /*$scope.Edit = function (obj, ev) {

         $scope.key = obj;
         $scope.showDialog(ev)
         }*/

        // $scope.init = function () {
        //     $scope.getlist(0)
        // }

        //$scope.getlist = function () {
            var _obj = {};
            $http.get(config.apiUrl + 'specimenTemplates')
                .then(function (response) {
                    $scope.specimenTemplatesList = response.data;
                })
        //};
        /*$scope.reset = function(){
            $scope.search = [];
        }*/

        $scope.remove = function (items, item) {
            var index = items.indexOf(item);
            items.splice(index, 1);
        };



        $scope.Delete = function (ev,items, item) {
            //$scope.remove(items, item)
            var confirm = $mdDialog.confirm()
                .title('Would you like to delete?')
                .targetEvent(ev)
                .ok('ok !')
                .cancel('cancel');

            $mdDialog.show(confirm).then(function() {
                //if yes/Ok
                $http.delete(config.apiUrl + "specimenTemplates/" + item.id)
                    .then(function (response) {
                        $scope.remove(items, item);
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('successfully Deleted')
                                .position('top right')
                                .hideDelay(3000)
                        );
                    })
            });
        };

    }]);
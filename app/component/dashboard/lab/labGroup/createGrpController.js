/**
 * Created by spider on 1/7/2018.
 */
angular.module('nproject')
    .controller('createGrpCtrl',['$scope', '$http', 'config','$mdToast','$stateParams','$state', function ($scope, $http, config, $mdToast, $stateParams, $state) {
        $scope.buttonDisable=false;


        if ($stateParams.id) {
            $http.get(config.apiUrl + 'specimenGroups/' + $stateParams.id)
                .then(function(response){
                    $scope.key = response.data;
                })
        }


        $scope.enableBtn = true;
        $scope.save =function () {
            $scope.enableBtn = false;
            if($scope.key){
                if ($stateParams.id) {
                    $http.put(config.apiUrl + "specimenGroups/" + $scope.key.id, $scope.key)
                        .then(function (response) {
                            $scope.data = response.data;
                            $mdToast.show(
                                $mdToast.simple()
                                    .textContent('successfully saved')
                                    .position('top right')
                                    .hideDelay(3000)
                            );

                            return response.data;
                        });
                } else  {
                    $http.post(config.apiUrl + "specimenGroups", $scope.key)
                        .then(function (response) {
                            $scope.disableBtn = true;
                            $scope.data = response.data;

                            $mdToast.show(
                                $mdToast.simple()
                                    .textContent('successfully saved')
                                    .position('top right')
                                    .hideDelay(3000)
                            );


                            return response.data;

                        });
                }
                $state.go("root.dashboard.labGroup", {}, { reload: true });
            }
        }
    }]);
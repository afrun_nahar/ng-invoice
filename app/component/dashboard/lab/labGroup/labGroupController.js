/**
 * Created by spider on 12/28/2017.
 */
angular.module('nproject')
    .controller('labGroupCtrl',['$scope', '$http', 'config', '$mdDialog','$mdToast', function ($scope, $http, config, $mdDialog,$mdToast) {



        // $scope.init = function () {
        //
        //     $scope.getlist(0)
        // }
        // $scope.getlist = function () {
            var _obj = {};
            //setTimeout(() => {
                $http.get(config.apiUrl + 'specimenGroups',{cache:false})
                .then(function (response) {
                    $scope.specimenGroupsList = response.data;
                }) 
            //}, 3000);
            
        // };
        $scope.reset = function(){
            $scope.search = [];
        };

        $scope.remove = function (items, item) {
            var index = items.indexOf(item);
            items.splice(index, 1);
        };

        $scope.Delete = function(ev, items, item) {
            // Appending dialog to document.body to cover sidenav in docs app
                var confirm = $mdDialog.confirm()
                    .title('Would you like to delete?')
                    .targetEvent(ev)
                    .ok('ok !')
                    .cancel('cancel');

            $mdDialog.show(confirm).then(function() {
                //if yes/Ok
                $http.delete(config.apiUrl + "specimenGroups/" + item.id)
                    .then(function (response) {
                        $scope.remove(items, item);
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('successfully Deleted')
                                .position('top right')
                                .hideDelay(4000)
                        );
                    })
            });
        };

        // $scope.showConfirm = function(ev) {
        //
        //     var confirm = $mdDialog.confirm()
        //         .title('Would you like to delete?')
        //         .targetEvent(ev)
        //         .ok('confirm !')
        //         .cancel('not confirm');
        //
        //     $mdDialog.show(confirm).then(function() {
        //         $scope.Delete = function (items, item) {
        //             //$scope.remove(items, item)
        //             $http.delete(config.apiUrl + "specimenGroups/" + item.id)
        //                 .then(function (response) {
        //                     $scope.remove(items, item);
        //                     $mdToast.show(
        //                         $mdToast.simple()
        //                             .textContent('successfully Deleted')
        //                             .position('top right')
        //                             .hideDelay(3000)
        //                     );
        //
        //                     return response.data;
        //                 });
        //         };
        //     });
        // };






    }]);
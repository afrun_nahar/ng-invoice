/**
 * Created by spider on 12/26/2017.
 */
angular.module('nproject')
    .controller('reportPrintCtrl',['$scope', '$http', 'config', '$stateParams', '$filter', '$window', function ($scope, $http, config, $stateParams, $filter, $window) {
        $scope.reportId = $stateParams.reportId;
        //$scope.specimenInfoId = $stateParams.reportId;
        if($stateParams.receiptId){
            $http.get(config.apiReceiptsUrl + 'specimenReceipts/' + $stateParams.receiptId)
                .then(function(response){
                    $scope.main = response.data;
                    return response.data;
                })
                .then(function(response){
                    var resArr = $filter('filter')(response.specimenReport, {id: $scope.reportId}, true)[0];
                    $scope.results = resArr;
                    //$scope.results.specimenResult.advice = "dg ↵dsf ↵sdf ↵dsf ↵sdf ↵dsf ↵sdf ↵dsf ↵sdf";
                    //$scope.results.specimenReport["0"].specimenResult.advice = "dg ↵dsf ↵sdf"; 
                })
            // .then(function(){
            //     setTimeout(function(){
            //         $window.print();
            //     }, 200);
            // });
        }

        $scope.deuAmount = function(amt){
                if (amt) {
                    if (amt < 0) {
                        return Math.abs(amt);
                    } else {
                        return amt;
                    }
                }
            }
    }]);
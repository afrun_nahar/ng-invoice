/**
 * Created by spider on 12/26/2017.
 */
angular.module('nproject')
    .controller('labReportListCtrl',['$scope', '$http', 'config', '$mdSidenav', '$mdToast', 
        function ($scope, $http, config, $mdSidenav, $mdToast) {

        $scope.src = {};

        $scope.Reset = function(){
            $scope.src = {};
            //$scope.selectedReceiptNo = null;
            $scope.searchReportText = "";
            // $scope.searchTextSpecimen = "";
        };

        // resourceCompanyProfile.get().then(function(res){
        //     $scope.profile = res.data;
        // })

        // $scope.specimenSearch = function(_txtSrc){
        //     return $http.get(config.apiUrl + 'autocompletes/specimen/' + _txtSrc)
        //         .then(function(result){
        //             return result.data;
        //         })
        // }

            $scope.enableBtn = false;

        $scope.reportSearch = function(_txtSrc){
            return $http.get(config.apiReceiptsUrl + 'autocompletes/report/' + _txtSrc)
                .then(function(result){
                    return result.data;
                });
        };
            $scope.enableBtn = true;
        //$scope.selectedItem = $scope.item;
        // $scope.$watch('specimenItem', function(newval, oldval){
        //     if (newval) {
        //         $scope.src.specimenId = newval.id;
        //     }
        // })
        //$scope.selectedItem = $scope.item;
        $scope.$watch('reportItem', GetTemplate);

        function GetTemplate(newval, oldval){
            if (newval) {
                //$scope.src.reportNo = newval.id;
                //$http.get(config.apiReceiptsUrl + 'specimenresults/report/' + newval.id)
                $http.get(config.apiReceiptsUrl + 'specimenreports/' + newval.id)
                    .then(function(result){
                        $scope.template = result.data;
                    });
            }
        }
        

        // $scope.Search = function(){
        //     if ($scope.src.reportNo) {
        //         $http.get(config.apiReceiptsUrl + 'specimenresults/' + $scope.src.reportNo)
        //             .then(function(result){
        //                 $scope.template = result.data;
        //             })
        //     } else {
        //         if ($scope.src.specimenId) {
        //             $http.get(config.apiUrl + 'specimenTemplates/specimen/' + $scope.src.specimenId)
        //                 .then(function(result){
        //                     $scope.template = result.data;
        //                 })
        //         }
        //     }
        // }

        $scope.Save = function(){

            var data = $scope.template.specimenResult;
            data.specimen_ReceiptId = $scope.template.specimen_ReceiptId
            
            $http.put(config.apiReceiptsUrl + 'specimenresults/' + $scope.template.id, data)
                .then(function(result){
                    //$scope.template = result.data;
                    GetTemplate($scope.template, null)
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('successfully saved')
                            .position('top right')
                            .hideDelay(3000)
                    );

                });

                
        
        };

    }]);
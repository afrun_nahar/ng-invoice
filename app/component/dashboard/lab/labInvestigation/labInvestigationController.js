/**
 * Created by spider on 12/28/2017.
 */
angular.module('nproject')
    .controller('InvestigationCtrl',['$scope', '$http', 'config', '$mdToast','$mdDialog', function ($scope, $http, config, $mdToast,$mdDialog) {

        $http.get(config.apiUrl + 'specimens')
        .then(function (response) {
            $scope.specimenList = response.data;

        })
        

        $scope.remove = function (items, item) {
            var index = items.indexOf(item);
            items.splice(index, 1);
        };


        $scope.Delete = function (ev,items, item) {
            //$scope.remove(items, item)
            var confirm = $mdDialog.confirm()
                .title('Would you like to delete?')
                .targetEvent(ev)
                .ok('ok !')
                .cancel('cancel');

            $mdDialog.show(confirm).then(function() {
                //if yes/Ok
                $http.delete(config.apiUrl + "specimens/" + item.id)
                    .then(function (response) {
                        $scope.remove(items, item);
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('successfully Deleted')
                                .position('top right')
                                .hideDelay(3000)
                        );
                    })
            });
        };
            /*$http.delete(config.apiUrl + "specimens/" + item.id)
                .then(function (response) {
                    $scope.remove(items, item);
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('successfully Deleted')
                            .position('top right')
                            .hideDelay(3000)
                    );

                    return response.data;
                });
        };*/
    }]);
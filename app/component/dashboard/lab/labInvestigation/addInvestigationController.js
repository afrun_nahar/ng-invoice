/**
 * Created by spider on 12/28/2017.
 */
angular.module('nproject')
    .controller('addInvestigationCtrl',['$scope', '$http', 'config','$mdToast','$stateParams','$state', function ($scope, $http, config, $mdToast,$stateParams,$state) {
        $scope.buttonDisable=false;
        $scope.key = { active:true };
        if ($stateParams.id) {
            $http.get(config.apiUrl + 'specimens/' + $stateParams.id)
                .then(function(response){
                    $scope.key = response.data;
                })
        }

        $scope.enableBtn = true;
        $scope.save =function () {
            $scope.enableBtn = false;

            if($scope.key){
                if ($stateParams.id) {
                    $http.put(config.apiUrl + "specimens/" + $scope.key.id, $scope.key)
                        .then(function (response) {
                            $scope.data = response.data;
                            $mdToast.show(
                                $mdToast.simple()
                                    .textContent('successfully saved')
                                    .position('top right')
                                    .hideDelay(3000)
                            );

                            return response.data;
                        });
                } else  {
                    $http.post(config.apiUrl + "specimens", $scope.key)
                        .then(function (response) {
                            $scope.data = response.data;
                            $mdToast.show(
                                $mdToast.simple()
                                    .textContent('successfully saved')
                                    .position('top right')
                                    .hideDelay(3000)
                            );

                            return response.data;
                        });
                }
                $state.go("root.dashboard.labInvestigation", {});

            }
        }
        /*$scope.save =function () {
            $scope.buttonDisable = true;
                $http.post(config.apiUrl + "specimens", $scope.key)
                    .then(function (response) {
                        $scope.data = response.data;
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('successfully saved')
                                .position('top right')
                                .hideDelay(3000)
                        );
                        return response.data;
                    });

        };*/
        $http.get(config.apiUrl + 'specimenGroups')
            .then(function (response) {
            $scope.specimenGrpsList = response.data;

        });
        $http.get(config.apiUrl + 'specimenStyles')
            .then(function (response) {
            $scope.specimenStyleList = response.data;

        });
        $http.get(config.apiUrl + 'specimenTitles')
            .then(function (response) {
            $scope.specimenTitleList = response.data;

        })


    }]);
/**
 * Created by spider on 1/6/2018.
 */
angular.module('nproject')
    .controller('invoiceDueCtrl', ['$scope', '$http', 'config', '$state', '$stateParams', '$mdToast', function ($scope, $http, config, $state, $stateParams, $mdToast) {

        if ($stateParams.id) {
            $http.get(config.apiReceiptsUrl + 'specimenReceipts/' + $stateParams.id)
                .then(function (response) {
                    $scope.labInvoiceDues = response.data;
                });
        }

        $scope.buttonDisable = false;
        $scope.save = function () {
            $scope.buttonDisable = true;

            var data = {
                specimen_ReceiptId: $stateParams.id,
                totalBillAmount: $scope.labInvoiceDues.totalBillAmount,
                //totalDiscountAmount: parseFloat($scope.labInvoiceDues.hosDisPerchentage) + parseFloat($scope.labInvoiceDues.adjustmentDiscount),
                previousReceivedAmount: $scope.labInvoiceDues.receivedAmount,
                currentReceivedAmount: $scope.currentReceivedAmount,
                remarks: $scope.remarks,
                totalDiscount: $scope.labInvoiceDues.totalDiscount
            }
            
            $http.post(config.apiReceiptsUrl + "dues", data)
                .then(function (response) {
                    //$scope.data = response.data;
                    //$scope.cancel();
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('successfully saved')
                            .position('top right')
                            .hideDelay(3000)
                    );
                    $scope.buttonDisable = false;
                    goprintandlist(response.data.id, function(){
                        $state.go('root.dashboard.printDueCollection');
                    });

                    return response.data;
                });
        }

        function goprintandlist(_id, callback){
            window.open('#/dashboard/printDueCollection/'+_id,'_blank');
            callback()
        }

        


    }]);
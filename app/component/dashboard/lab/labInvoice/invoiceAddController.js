/**
 * Created by spider on 12/20/2017.
 */
angular.module('nproject')
    .controller('invoiceCtrl', ['$scope', '$http', 'config', '$mdDialog', '$filter', '$mdToast', '$state', '$stateParams', 'labCalculationFactory'
    , function ($scope, $http, config, $mdDialog, $filter, $mdToast, $state, $stateParams, labCalculationFactory) {
        
        // var tomorrow = new Date();
        // tomorrow.setDate(new Date().getDate()+1);
        
        $scope.main = {
             patientInfo: {
                 sex: 'Male'
             }
            //, deliveryDateTime: new Date(tomorrow)
            , specimenReport: [newSpecimenReport()]
            , hosDisPerchentage: 0
            , adjustmentDiscount: 0
            , receivedAmount: 0
        };

        $scope.existingPatient = false;
        $scope.existingHodpital = false;
        $scope.existingDoctor = false;

        $scope.refreshObject = function(obj, mainId){
            if (obj) {
                if (obj.id) {
                    delete obj.id;
                }
                if (mainId) {
                    delete $scope.main[mainId];
                }
            }
        }

        AgeFunc().get('00-00-00');

        



        if ($stateParams.id) {
            $http.get(config.apiReceiptsUrl + 'specimenReceipts/' + $stateParams.id)
                .then(function (response) {
                    $scope.main = response.data;
                    $scope.main.deliveryDateTime = new Date(response.data.deliveryDateTime)
                    return response.data;
                })
                .then(function (response) {
                    var patient = response.patientInfo;
                    var hospital = response.referralInfo;
                    var doctor = response.doctorInfo;

                    $scope.selectedPatient = patient;// {id: patient.id, name: patient.name};
                    $scope.selectedHospital = hospital; //{id: hospital.id, name: hospital.name};
                    $scope.selectedDocotr = doctor; //{id: doctor.id, name: doctor.name};

                    $scope.existingPatient = true;
                    $scope.existingHodpital = true;
                    $scope.existingDoctor = true;
                    AgeFunc(patient.age).get();
                })
        }

        $scope.showDialog = function (ev) {
            $mdDialog.show({
                controller: patientDialogCtrl,
                templateUrl: 'component/dashboard/lab/labInvoice/patientDialog/patientDialog.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true
            })
        };

        function patientDialogCtrl($scope, $mdDialog) {

            $scope.cancel = function () {
                $mdDialog.cancel();
            };

            $scope.answer = function (answer) {
                $mdDialog.hide(answer);
            };
        }

        $scope.answer = function (answer) {
            $mdDialog.hide(answer);
        };

        function AgeFunc(){
            return {
                setTo: function(mdl){
                    return $scope.ageYear + '-' + $scope.ageMonth + '-' + $scope.ageDay;
                }
                , get: function(date){
                    if (date) {
                        var ageArray = date.split('-');
                        $scope.ageDay = parseInt(ageArray[2]);
                        $scope.ageMonth = parseInt(ageArray[1]);
                        $scope.ageYear = parseInt(ageArray[0]);
                    }
                }
            }
        }

        // $scope.$watch('ageDay', function(newVal, oldVal){
        //     if (newVal) {
        //         var age = $scope.main.patientInfo.age || '00-00-00';
        //         var ageArray = age.split('-');
        //         ageArray[0] = newVal;
        //         $scope.main.patientInfo.age = ageArray[0] + '-' + ageArray[1] + '-' + ageArray[2];
                
        //     }
        // })
        // $scope.$watch('ageMonth', function(newVal, oldVal){
        //     if (newVal) {
        //         var age = $scope.main.patientInfo.age || '00-00-00';
        //         var ageArray = age.split('-');
        //         ageArray[1] = newVal;
        //         $scope.main.patientInfo.age = ageArray[0] + '-' + ageArray[1] + '-' + ageArray[2];
        //     }
        // })
        // $scope.$watch('ageYear', function(newVal, oldVal){
        //     if (newVal) {
        //         var age = $scope.main.patientInfo.age || '00-00-00';
        //         var ageArray = age.split('-');
        //         ageArray[2] = newVal;
        //         $scope.main.patientInfo.age = ageArray[0] + '-' + ageArray[1] + '-' + ageArray[2];
        //         $scope.ageDay = ageArray[0];
        //         $scope.ageMonth = ageArray[1];
        //     }
        // })

        //$scope.key = newSpecimenReport();

        function newSpecimenReport() {
            return {
                specimen_InfoId: '',
                charge: 0
                , discountPerchentage: 0
                , specialDiscount: 0
                , deliveryDateTime: new Date()
            };
        }

        //$scope.items = [$scope.key];

        $scope.addButton = function () {
            $scope.main.specimenReport.push(newSpecimenReport());
        };

        // $scope.$watch('selectedPatient', function (newVal, oldVal) {
        //     if (newVal) {
        //         $scope.main.patientInfo = newVal;
        //     }
        //     //console.log('patient watch : ', newVal)
        // })
        // $scope.$watch('selectedHospital', function (newVal, oldVal) {
        //     if (newVal) {
        //         $scope.main.referralInfo = newVal;
        //     }
        // })
        $scope.changePatient = function(item){
            $scope.main.patient_InfoId = item.id;
            $scope.main.patientInfo = item;
            AgeFunc().get(item.age);
        };
        $scope.changeReffered = function(item){
            $scope.main.referral_InfoId = item.id;
            $scope.main.referralInfo = item;
        };
        $scope.changeDocotor = function(item){
            $scope.main.doctor_InfoId = item.id;
            $scope.main.doctorInfo = item;
        };
        $scope.changeMarketingInfo = function(item){
            $scope.main.referral_Marketing_InfoId  = item.id;
            $scope.main.referralMarketingInfo = item;
        };
        // $scope.$watch('selectedDocotr', function (newVal, oldVal) {
        //     if (newVal) {
        //         $scope.main.doctorInfo = newVal;
        //     }
        // });


        $scope.querySearch = function (method, _txtSrc) {
            return $http.get(config.apiUrl + "autocompletes/" + method + "/" + _txtSrc)
                .then(function (result) {
                    return result.data;
                })
        };

        $scope.calc = labCalculationFactory;

        // $scope.getTotal = function () {
        //     // var total = $filter('calculateBy')($scope.main.specimenReport, 'charge') || 0;
        //     // //var totalDiscountInTaka = total * ( $filter('calculateBy')($scope.main.specimenReport, 'discountPerchentage') / 100 ) || 0;
        //     // var totalDiscountInTaka =  ( $filter('calculateBy')($scope.main.specimenReport, 'discountPerchentage') / 100 ) || 0;
        //     // var totalSpecialDiscount = $filter('calculateBy')($scope.main.specimenReport, 'specialDiscount') || 0;
        //     // var totalDiscount = totalDiscountInTaka + totalSpecialDiscount;
        //     //return total - totalDiscount || 0;
        //     var netTotal = 0;
        //     for (let i = 0; i < $scope.main.specimenReport.length; i++) {
        //         const element = $scope.main.specimenReport[i];
        //         netTotal += labCalculationFactory.Row(element.charge, element.discountPerchentage, element.specialDiscount).NetTotal;
        //         //netTotal += calculateCashMemo(element.charge, element.discountPerchentage, element.specialDiscount);
        //     }
        //     return netTotal;

        // }


        // $scope.getTotalDiscount = function () {
        //     //var total = $scope.getTotal();
        //     //var hospitalDiscount = parseFloat($scope.main.hosDisPerchentage / 100 * total);
        //     //var totalDiscount = hospitalDiscount + parseFloat($scope.main.adjustmentDiscount);
        //     return parseFloat($scope.main.adjustmentDiscount);
        // }

        // $scope.getTotalReceivable = function () {
        //     var total = labCalculationFactory.TotalBill($scope.main.specimenReport);
        //     return total - labCalculationFactory.TotalDiscount($scope.main.specimenReport);
        // }

        $scope.getDueAmount = function () {
            var receivable = $scope.calc.TotalReceivable($scope.main.specimenReport, $scope.main.adjustmentDiscount)// $scope.getTotalReceivable();
            var due = receivable - parseFloat($scope.main.receivedAmount);
            if (due < 0) {
                return 0;
            } else {
                return due;
            }
        }

        $scope.getChangeAmount = function () {
            var receivable = $scope.calc.TotalReceivable($scope.main.specimenReport, $scope.main.adjustmentDiscount)//  $scope.getTotalReceivable();
            var change = receivable - parseFloat($scope.main.receivedAmount);
            if (change > 0) {
                return 0;
            } else {
                return Math.abs(change);
            }
        }


        $scope.buttonDisable = false;
        $scope.save = function () {
            $scope.buttonDisable = true;

            var primaryId = $stateParams.id;

            $scope.main.patientInfo.age = AgeFunc().setTo()

            var opts = {
                method: primaryId ? "PUT" : "POST",
                url: config.apiReceiptsUrl + "specimenReceipts" + (primaryId ? "/" + primaryId : ""),
                data: $scope.main
            };
            //$http(.post(config.apiReceiptsUrl + "specimenReceipts", $scope.main))
            $http(opts)
                .then(function (response) {
                    $scope.buttonDisable = false;
                    //$scope.main = response.data;
                    
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent("Successfully " + (primaryId ? "updated" : "saved"))
                            .position('top right')
                            .hideDelay(3000)
                    );

                    goprintandlist(response.data.id, function(){
                        $state.go('root.dashboard.labInvoice', { });
                    });
                    
                }, function () {
                    $scope.buttonDisable = false;
                });
        }


        function goprintandlist(_id, callback){
            window.open('#/dashboard/printInvoice/' + _id,'_blank');
            callback()
        }
		
		$scope.createNew = function(ev){
			var confirm = $mdDialog.confirm()
                .title('Would you like to create new onece?')
                .targetEvent(ev)
                .ok('ok !')
                .cancel('cancel');

            $mdDialog.show(confirm).then(function() {
                //if yes/Ok
                $state.go("root.dashboard.createLabInvoice", {id: ''});
            });
		}
        

    }]);

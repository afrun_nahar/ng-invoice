/**
 * Created by spider on 1/6/2018.
 */
(function(){
    angular.module('nproject')
    .controller('dueCollectionListCtrl', dueCollectionFunc);

    function dueCollectionFunc($scope, $http, config, $filter, customDate) {
        
        $scope.src = {
            sDate:  new Date()
            , eDate: new Date()
        };

        $scope.Search = function(){
            var str = "";
            //var _date =  $filter('date')($scope.src.date, 'yyyy-MM-dd');
            if ($scope.src.receiptNo) {
                str = "receipt/" + $scope.src.receiptNo;
            } else {
                if ($scope.src.sDate) {
                    if ($scope.src.eDate) {
                        str = "fromDate/"+customDate.parseDate($scope.src.sDate)+"/toDate/"+customDate.parseDate($scope.src.eDate)+"/pageNumber/1/pageSize/50";    
                    }
                }
            }

            $http.get(config.apiReceiptsUrl + 'dues/' + str)
            .then(function (response) {
                $scope.duesList = response.data;
            });
        };
        $scope.Search();

        // function parseDate(_date){
        //     return $filter('date')(_date, 'yyyy-MM-dd');
        // }

        $scope.ReceiptQuerySearch = function(method, _txtSrc){
            return $http.get(config.apiReceiptsUrl + "autocompletes/"+ method + "/" + _txtSrc)
                .then(function(result){
                    return result.data;
                })
        }

        $scope.$watch('selectedReceiptNo', function(newVal, oldVal){
            if (newVal) {
                $scope.src.receiptNo = newVal.id;
            }
        })
        
    }
})()

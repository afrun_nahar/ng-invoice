/**
 * Created by spider on 12/23/2017.
 */
angular.module('nproject')
    .controller('invoiceListCtrl',['$scope', '$http','$mdSidenav', '$filter', 'config', '$mdDialog', '$mdToast', function ($scope, $http, $mdSidenav, $filter, config, $mdDialog, $mdToast) {
        $scope.toggleLeft = buildToggler('search-nav');

        function buildToggler(componentId) {
            return function () {
                $mdSidenav(componentId).toggle();
            };
        }

        
        $scope.toDate = new Date();
        $scope.fromDate = new Date();

        $scope.src = defaultSearchValue();

        function defaultSearchValue(){
            return {
                pageNumber: 1,
                pageSize: 20,
                receiptNo: '',
                refferedId: ''
            }
        }

        $scope.Reset = function(){
            $scope.src = defaultSearchValue();
            $scope.selectedReceiptNo = null;
            $scope.selectedDocotr = null;
        }
        

        //$http.get(config.apiReceiptsUrl + 'specimenReceipts/fromDate/2017-12-01/toDate/2018-01-30/pageNumber/1/pageSize/20')

        Search();
        $scope.Search = function(){
            Search(function(){
                $scope.toggleLeft();
            });
        };

        function Search(callback){
            var str = "";
            if ($scope.src.receiptNo) {
                str = $scope.src.receiptNo;
            } else {
                if ($scope.src.refferedId) {
                    srt = "doctor/" + $scope.src.refferedId;
                } else {
                    $scope.src.toDate = $filter('date')($scope.toDate, 'yyyy-MM-dd');
                    $scope.src.fromDate = $filter('date')($scope.fromDate, 'yyyy-MM-dd');
                    str = 'fromDate/'+$scope.src.fromDate+'/toDate/'+$scope.src.toDate+'/pageNumber/'+$scope.src.pageNumber+'/pageSize/' + $scope.src.pageSize
                }
            }

            //$http.get(config.apiReceiptsUrl + 'specimenReceipts/fromDate/2017-12-01/toDate/2018-01-30/pageNumber/1/pageSize/20')
            $http.get(config.apiReceiptsUrl + "specimenReceipts/" + str)
                .then(function(response){
                    if (Array.isArray(response.data)) {
                        $scope.labInvoiceList = response.data;
                    } else {
                        $scope.labInvoiceList = [response.data];
                    }
                    if (callback != undefined || callback == 'function') {
                        callback();
                    }
                });
        }

        //$scope.Search();
        
        $scope.querySearch = function(method, _txtSrc){
            return $http.get(config.apiUrl + "autocompletes/"+ method + "/" + _txtSrc)
                .then(function(result){
                    return result.data;
                });
        };
        
        $scope.ReceiptQuerySearch = function(method, _txtSrc){
            return $http.get(config.apiReceiptsUrl + "autocompletes/"+ method + "/" + _txtSrc)
                .then(function(result){
                    return result.data;
                });
        };

        $scope.$watch('selectedDocotr', function(newVal, oldVal){
            if (newVal) {
                $scope.src.refferedId = newVal.id;
            }
        });

        $scope.$watch('selectedReceiptNo', function(newVal, oldVal){
            if (newVal) {
                $scope.src.receiptNo = newVal.id;
            }
        });
		
		$scope.remove = function (items, item) {
            var index = items.indexOf(item);
            items.splice(index, 1);
        };
		
		$scope.Delete = function (ev,items, item) {
            //$scope.remove(items, item)
            var confirm = $mdDialog.confirm()
                .title('Would you like to delete?')
                .targetEvent(ev)
                .ok('ok !')
                .cancel('cancel');

            $mdDialog.show(confirm).then(function() {
                //if yes/Ok
                $http.delete(config.apiReceiptsUrl + "specimenReceipts/" + item.id)
                    .then(function (response) {
                        $scope.remove(items, item);
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('successfully Deleted')
                                .position('top right')
                                .hideDelay(3000)
                        );
                    })
            });
        };

        
    }]);
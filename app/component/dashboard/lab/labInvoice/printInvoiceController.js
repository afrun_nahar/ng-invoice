/**
 * Created by spider on 12/24/2017.
 */
angular.module('nproject')
    .controller('printInvoiceCtrl', ['$scope', '$http', 'config', '$stateParams', '$window',
    function ($scope, $http, config, $stateParams, $window) {
        if ($stateParams.id) {
            // resourceCompanyProfile.get().then(function(res){
            //     $scope.profile = res.data;
            // })
            $http.get(config.apiReceiptsUrl + 'specimenReceipts/' + $stateParams.id)
                .then(function (response) {
                    $scope.main = response.data;
                })
                 .then(function () {

                     setTimeout(function () {
                         $window.print();
                     }, 200);
                 })


        }
        
        $scope.rowNetTotal = function (total,discount,adjDiscount) {
            return (calculateCashMemo(total,discount,adjDiscount)).toFixed(2);
        };
        function calculateCashMemo(total,discount,adjDiscount){
            var total = total;
            var adjDiscount = AdjDiscount(total,discount,adjDiscount);
            return total-adjDiscount;
        }
        function discountIntTaka(amount){
            return amount/100;
        }
        function AdjDiscount(charge,discount,adjAmount){
            var totalAfterDiscount = (charge*discountIntTaka(discount));
            var calc = totalAfterDiscount + adjAmount;
            return calc;
        }
        $scope.deuAmount = function (amt) {
            if (amt) {
                if (amt < 0) {
                    return Math.abs(amt);
                } else {
                    return amt;
                }
            }
        }
    }]);
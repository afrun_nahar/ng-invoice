/**
 * Created by spider on 1/16/2018.
 */
angular.module('nproject')
    .controller('addAuthUserCtrl',['$scope', '$http','$state','config','$stateParams','$mdToast',
        function ($scope, $http, $state, config,$stateParams,$mdToast) {
        $scope.cancel = function () {
            $state.go("root.dashboard.authorisation.authUserRole", {}, { reload: true });
        }
        $scope.enableBtn = true;
        $scope.save = function () {
            $scope.enableBtn = false;

            var primaryId = $stateParams.id;

            var opts = {
                method: primaryId ? "PUT" : "POST",
                url: config.authUrl + "UserRoles/Save" + (primaryId ? "/" + primaryId : ""),
                data: $scope.main
            };

            $http(opts)
                .then(function (response) {
                    $scope.buttonDisable = false;
                    //$scope.main = response.data;

                    $mdToast.show(
                        $mdToast.simple()
                            .textContent("Successfully " + (primaryId ? "updated" : "saved"))
                            .position('top right')
                            .hideDelay(3000)
                    );


                }, function () {
                    $scope.buttonDisable = false;
                });
        }
        $http.get(config.authUrl + 'Roles/Get')
            .then(function (response) {
                $scope.authRoleList = response.data;

            });
        $http.get(config.authUrl + 'User/Get')
            .then(function (response) {
                $scope.authUserlist = response.data;

            });

    }]);
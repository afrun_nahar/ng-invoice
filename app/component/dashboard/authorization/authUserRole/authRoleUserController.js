/**
 * Created by spider on 1/14/2018.
 */
angular.module('nproject')
    .controller('authRoleUserCtrl',['$scope', '$http', 'config',function ($scope, $http, config) {
        $http.get(config.authUrl + 'UserRoles/GetUserRoles',{cache:false})
            .then(function (response) {
                $scope.authRoleList = response.data;

            })
    }]);
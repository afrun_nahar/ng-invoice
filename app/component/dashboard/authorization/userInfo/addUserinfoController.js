
/**
 * Created by spider on 1/14/2018.
 */
angular.module('nproject')
    .controller('addUserInfoCtrl',['$scope', '$http','$state','config','$stateParams', function ($scope, $http, $state, config, $stateParams) {


        $scope.enableBtn = true;
        $scope.save =function () {
            $scope.enableBtn = false;
            if($scope.main){
                if ($stateParams.id) {
                    $http.put(config.authUrl + "User/Save" + $scope.main.id, $scope.main)
                        .then(function (response) {
                            $scope.data = response.data;
                            $mdToast.show(
                                $mdToast.simple()
                                    .textContent('successfully updated')
                                    .position('top right')
                                    .hideDelay(3000)
                            );

                            return response.data;
                        });
                } else  {
                    $http.post(config.authUrl + "User/Save", $scope.main)
                        .then(function (response) {
                            $scope.disableBtn = true;
                            $scope.data = response.data;

                            $mdToast.show(
                                $mdToast.simple()
                                    .textContent('successfully saved')
                                    .position('top right')
                                    .hideDelay(3000)
                            );


                            return response.data;

                        });
                }
                $state.go("root.dashboard.authorisation.createUserInfo", {}, { reload: true });
            }
        }
        $scope.cancel = function () {
            $state.go("root.dashboard.authorisation.userInfo", {}, { reload: true });
        }

    }]);
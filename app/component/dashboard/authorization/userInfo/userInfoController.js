/**
 * Created by spider on 1/14/2018.
 */
angular.module('nproject')
    .controller('userInfoCtrl',['$scope', '$http', 'config',function ($scope, $http, config) {
        $http.get(config.authUrl + '/User/Get',{cache:false})
            .then(function (response) {
                $scope.userRoleList = response.data;

            })
    }]);
/**
 * Created by spider on 1/14/2018.
 */
angular.module('nproject')
    .controller('roleAuthCtrl',['$scope', '$http','config','$stateParams','$mdToast', function ($scope, $http, config, $stateParams,$mdToast) {

        $scope.key = {
            roleName: ''

        };

        function newObj() {
            return {roleName: ''}
        }

        $scope.items = [$scope.key];

        $scope.addButton = function() {
            $scope.items.push(newObj());
        };

        $http.get(config.authUrl + 'Roles/Get',{cache:false})
            .then(function (response) {
                $scope.roleList = response.data;

            })

        $scope.enableBtn = true;
        $scope.save =function () {
            $scope.enableBtn = false;
            if($scope.key){
                if ($stateParams.id) {
                    $http.put(config.authUrl + "Roles/Save" + $scope.main.id, $scope.main)
                        .then(function (response) {
                            $scope.data = response.data;
                            $mdToast.show(
                                $mdToast.simple()
                                    .textContent('successfully updated')
                                    .position('top right')
                                    .hideDelay(3000)
                            );

                            return response.data;
                        });
                } else  {
                    $http.post(config.authUrl + "Roles/Save", $scope.main)
                        .then(function (response) {
                            $scope.disableBtn = true;
                            $scope.data = response.data;

                            $mdToast.show(
                                $mdToast.simple()
                                    .textContent('successfully saved')
                                    .position('top right')
                                    .hideDelay(3000)
                            );
                            return response.data;

                        });
                }

            }
        }

        // $scope.save = function () {
        //     $http.post(config.authUrl + '/Roles/Save',{cache:false})
        //         .then(function (response) {
        //             $scope.roleList = response.data;
        //
        //         })
        // }
    }]);
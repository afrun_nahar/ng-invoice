/**
 * Created by spider on 2/6/2018.
 */

(function(){
    angular.module('nproject')
    .controller('bundleDueCollectionCtrl', function ($scope, $http, config, $stateParams, notifyFactory, $state) {


            $scope.main={};
        $scope.querySearch = function (_txtSrc) {
            return $http.get(config.apiUrl +  "autocompletes/referral/"+ _txtSrc)
                .then(function (result) {
                    return result.data;
                })
        };
        $scope.changeReffered = function (item) {
            $scope.main.referralId = item.id;
            $http.get(config.apiReceiptsUrl +  "dueReceipts/referral/"+ item.id)
                .then(function (result) {
                    $scope.bundleList = result.data;
                });
            // console.log(item);
        }
        $scope.save = function () {
            $http.post(config.apiReceiptsUrl +  "bundleDueCollections", $scope.main)
                .then(function (reponse) {
                    $scope.main = reponse.data;
                    notifyFactory.success()
                }).then(function () {
                    setTimeout(function(){
                        $state.reload();
                    }, 1000);
                })
            // console.log(item);
        }
    });
})();


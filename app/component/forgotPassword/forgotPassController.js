/**
 * Created by spider on 1/29/2018.
 */
angular.module('nproject')
    .controller('forgotPassCtrl',['$scope', '$http','config','$mdToast','$stateParams', function ($scope, $http, config, $mdToast, $stateParams) {

        $scope.enableBtn = false;
        $scope.enableLoader = false;

           $scope.getEmail = function () {
               $scope.enableLoader = true;
               $scope.userMessage = {};
                   //setTimeout(function () {
                       $http.get(config.authUrl + 'Email/GetEmailAddress',
                           {params : { userName : $scope.user.userName }}, {cache:false})
                           .then(function (response) {
                               $scope.enableBtn = true;
                               $scope.enableLoader = false;
                               $scope.userMessage = response.data;

                               return response.data;

                           }, function (response) {
                               $scope.user = response;
                           });
                   //}, 2000)

               }


               $scope.sendEmail = function (UserName) {
                   $scope.disableBtn = true;

                   $http.post(config.authUrl + 'Email/Send',
                       { userName : UserName })
                       .then(function (response) {
                           $scope.enableBtn = false;
                           $mdToast.show(
                               $mdToast.simple()
                                   .textContent('successfully send, check your inbox')
                                   .position('top right')
                                   .hideDelay(3000)
                           );


                       });
               }


    }]);
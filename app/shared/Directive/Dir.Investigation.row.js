angular.module("commonDir")
.directive("invRow", ['$http', 'labCalculationFactory', function ($http, labCalculationFactory) {
    return {
        restrict: "AEC",
        scope: {
            item: '=',
            items: '='
        }
        , controller: function ($scope, $http, config) {
            $scope.querySearch = function(_txtSrc){
                return $http.get(config.apiUrl + 'autocompletes/specimen/' + _txtSrc)
                    .then(function(result){
                        return result.data;
                    })
            }

            $scope.remove = function(items, item){
                var index = items.indexOf(item);
                items.splice(index, 1);
            }
    
            $scope.Delete = function(items, item){
                // if (item.id) {
                //     $scope.buttonDisable = true;
                //     $http.delete(config.apiReceiptsUrl + "specimenReceipts/" + item.id, item)
                //         .then(function (response) {
                //             $scope.buttonDisable = false;
                //             $mdToast.show(
                //                 $mdToast.simple()
                //                     .textContent('successfully Delete')
                //                     .position('top right')
                //                     .hideDelay(3000)
                //             );
                //         });
                // } else {
                    $scope.remove(items, item);
                //}
                
            }

            $scope.AdjDiscount = function(){
                return labCalculationFactory.Row($scope.item).AdjDiscount
            }
            $scope.NetTotal = function(){
                return labCalculationFactory.Row($scope.item).NetTotal
            } 

            $scope.$watch('item', function(newval, oldval){
                if (newval) {
                    if (newval.specimen_InfoId) {
                        $scope.selectedItem = { id: newval.specimen_InfoId, specimen: newval.specimenInfo.specimen, charge: newval.charge };        
                    }
                }
            });

            //$scope.selectedItem = $scope.item;
            $scope.$watch('selectedItem', function(newval, oldval){
                if (newval) {
                    $scope.item.specimen_InfoId = newval.id;
                    $scope.item.charge = newval.charge;
                }
            });

            
        }
        , templateUrl: 'shared/template/Dir.Investigation.row.html'
    };
}])
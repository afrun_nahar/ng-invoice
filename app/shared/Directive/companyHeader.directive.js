angular.module("commonDir")
.directive("companyHeader", ['$http', function ($http) {
    return {
        restrict: "AEC",
         controller: function ($scope, resourceCompanyProfile) {
            resourceCompanyProfile.get().then(function(res){
                $scope.profile = res.data;
            })
        }
        , templateUrl: 'shared/template/companyHeader.directive.html'
    };
}])
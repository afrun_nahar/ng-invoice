angular.module('common')
    .factory('notifyFactory', ['$mdToast', function($mdToast){

        return {
            success: success
        }
        
        function success(message){
            this.msg = message;
            var a = new alert(this.msg || "Success!")
            a.message = this.msg || "Success!";
        }

        function alert(message){
            this.message = message;
            this.type = "success";
            $mdToast.show(
                $mdToast.simple()
                    .textContent(this.message)
                    .position('top right')
                    .hideDelay(3000)
                    .toastClass(this.type)
            );
        }
        
    }]);
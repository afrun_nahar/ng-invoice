/**
 * Created by DIU on 8/29/2017.
 */
angular.module('common')
    .factory('tokenFactory', ['config', tockenFactory])

function tockenFactory(config) {

    return {
        accessToken : function (_userName, _data) {
            localStorage.setItem("labToken", JSON.stringify({userName: _userName, token: "bearer " + _data }) );
        }

        , getToken : function () {
            var accToken = JSON.parse(localStorage.getItem("labToken"));
            if (accToken) {
                return accToken.token;
            }
            //return JSON.parse(localStorage.getItem("labToken")).token;
        }
        , getUser : function () {
            var accToken = JSON.parse(localStorage.getItem("labToken"));
            if (accToken) {
                return accToken.userName;
            }
            //return JSON.parse(localStorage.getItem("labToken")).token;
        }

        , removeToken : function () {
            return localStorage.removeItem("labToken");
        }
        

    }


}
/**
 * Created by DIU on 8/29/2017.
 */
angular.module('common')
 .factory('httpRequestInterceptor',['tokenFactory', '$injector', function (tokenFactory, $injector) {
     return {
         request: function (config) {
             config.headers['Authorization'] = tokenFactory.getToken();
             //config.headers['Accept'] = 'application/json';
             //config.headers['Content-Type'] = 'application/json';
                //console.log('query start....', config)
             return config;
         }
         // , response: function (config) {
         //     console.log('interceptor ', config);
         //     // if(config.config.headers['Content-Type']){
         //     //     console.log('for xhr ', config.config.method, config.config)
         //     // }
         //     //if not html
         //     if(config.config.method == "GET"){
         //         if(config.config.headers.Accept ){
         //             if(config.config.headers.Accept.indexOf("application/json") !== -1 ){
         //                 console.log('query success....')
         //             }
         //         }
         //     }
         //
         //     $injector.get('notifyFactory').show(config);
         //     return config;
         // }
         //
         // , responseError: function (config) {
         //     console.log('responseError ', config)
         //     $injector.get('notifyFactory').show(config);
         //     return config;
         // }
     };
}]);
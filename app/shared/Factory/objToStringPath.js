
angular.module('common')
    .factory('objtostringpath', [function () {
        var fc = this;
        fc.getParamsString = function(_obj) {
            
            var countObjectKeys = Object.keys(_obj);
            var qryStr = "";
            for (var i = 0; i < countObjectKeys.length; i++) {
                if(_obj[countObjectKeys[i]]){
                    qryStr += countObjectKeys[i] + "/" + _obj[countObjectKeys[i]];

                    if(i != countObjectKeys.length - 1){
                        qryStr += "/";
                    }
                }
            }
            return qryStr;
        };

        fc.getParamsStringByOrder = function(orderKeys,_obj) {
            
            var countObjectKeys = Object.keys(_obj);
            var listedObj = {};
            if (orderKeys.length == countObjectKeys.length) {
                for (var i = 0; i < orderKeys.length; i++) {
                    if (_obj[orderKeys[i]]) {
                        listedObj[orderKeys[i]] = _obj[orderKeys[i]];    
                    }
                    
                }
            }

            
            return fc.getParamsString(listedObj);
        };

        return fc;
}])

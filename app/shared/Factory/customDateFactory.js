(function(){
    angular.module('common')
        .factory('customDate', dateFunc);


        function dateFunc($filter){

            return {
                parseDate: parseDate
            }
            
            function parseDate(_date){
                return $filter('date')(_date, 'yyyy-MM-dd')
            }
            
        }
})();
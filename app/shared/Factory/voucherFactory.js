angular.module('common')
.factory('voucherFactory', vFactory)

function vFactory($http, config, $mdToast, $window, notifyFactory) {

    //Class: Journal
    function Journal(type, codeId, particulars, amount){
        this.gL_Account_CodeId = codeId;
        this.type = type;
        this.particulars = particulars;
        this.amount= amount;
        return {
            gL_Account_CodeId: this.gL_Account_CodeId,
            particulars: this.particulars,
            dr: this.type == 'dr' ? this.amount : 0,
            cr: this.type == 'cr' ? this.amount : 0
        };
    };
    //Class: Voucher
    function Voucher(vType){
        return {
            voucherType: vType,
            note: '',
            voucherSub: [ Journal('dr', '', '', 0), Journal('cr', '', '', 0) ]
        };
    }

    //Class: PaymentVoucher
    function PaymentVoucher(vType){
        return {
            voucherType: vType,
            note: '',
            voucherSub: {cr:  [Journal('cr', '5', 'Cash In Hand', 0)], dr: [ Journal('dr', '', '', 0) ]}
        };
    }

    function voucherController(_type){
        var vm = this;
        
        return {
            Voucher: Voucher(_type || 'JV'),
            changeAutocompleteValue : function(item, items, index) {
                //console.log(item, index)
                items[index].gL_Account_CodeId = item.id;
                items[index].particulars = item.glAccountName;
            },
            querySearch : function (_txtSrc) {
                return $http.get(config.apiAccountsUrl + "accountHeads/" + _txtSrc)
                    .then(function (result) {
                        return result.data;
                    });
            },
            addRow: function(type, items) {
                //this.Voucher.voucher_Sub.push(
                items.push(
                    Journal(type, '', '', 0)
                );
            },
            remove: function(items, item){
                var index = items.indexOf(item);
                items.splice(index, 1);
            },
            reload: function(){
                //$state.reload();
                $window.location.reload()
            }
            , Save: function (_data) {
                this.primaryId = "";
                this.data = _data;
                var opts = {
                    method: this.primaryId ? "PUT" : "POST",
                    url: config.apiAccountsUrl + "journals" + (this.primaryId ? "/" + this.primaryId : ""),
                    //data: this.Voucher
                    data: this.data
                };
                $http(opts)
                .then(function (response) {
                    //$scope.main = response.data;
                    
                    // $mdToast.show(
                    //     $mdToast.simple()
                    //         .textContent("Successfully " + (this.primaryId ? "updated" : "saved"))
                    //         .position('top right')
                    //         .hideDelay(3000)
                    // );

                    notifyFactory.success("Successfully " + (this.primaryId ? "updated" : "saved"))

                    setTimeout(function(){
                        $window.location.reload()
                    }, 1000);
                    
    
                    // goprintandlist("20180100014", function(){
                    //     $state.go('root.dashboard.labInvoice', { });
                    // });
                    
                }, function () {
                    //$scope.buttonDisable = false;
                });
                
            }
            // , Notification : function(){
            //     $mdToast.show(
            //         $mdToast.simple()
            //             .textContent("Successfully " + (this.primaryId ? "updated" : "saved"))
            //             .position('top right')
            //             .hideDelay(3000)
            //     );
            // }
        };
    }

    return {
        Journal : Journal,
        Voucher : Voucher,
        PaymentVoucher: PaymentVoucher,
        VoucherController: voucherController
    };


}
(function(){


angular.module('common')
    .factory('labCalculationFactory', function($filter){

        return {
            TotalBill: function(items){
                return $filter('calculateBy')(items, 'charge') || 0;
            }
            , Row: function(item){
                return {
                    Total: item.charge
                    , Discount: discountIntTaka(item.discountPerchentage)
                    , AdjDiscount: AdjDiscount(item.charge, item.discountPerchentage, item.specialDiscount)
                    , NetTotal: calculateCashMemo(item.charge, item.discountPerchentage, item.specialDiscount)
                }
            }
            , NetTotalBill: function(items){
                var netTotal = 0;
                for (var i = 0; i < items.length; i++) {
                    var element = items[i];
                    netTotal += calculateCashMemo(element.charge, element.discountPerchentage, element.specialDiscount);
                }
                return netTotal;
            }
            , TotalAdjustedDiscount: function(items, admustment){
                var discountTotal = 0;
                for (var i = 0; i < items.length; i++) {
                    var element = items[i];
                    discountTotal += AdjDiscount(element.charge, element.discountPerchentage, element.specialDiscount);
                }
                var discountTotal = discountTotal + parseFloat(admustment);
                var fixVal = parseFloat(discountTotal.toFixed(2))
                return fixVal;
            }
            , TotalReceivable: function(items, adjustmentDiscount){
                return this.TotalBill(items) - this.TotalAdjustedDiscount(items, adjustmentDiscount);
            }
        }
        
        function calculateCashMemo(total,discount,adjDiscount){
            var total = total;
            var adjDiscount = AdjDiscount(total,discount,adjDiscount);
            return parseFloat((total-adjDiscount).toFixed(2));
        }
        function discountIntTaka(amount){
            return amount/100;
        }
        // function TotalDiscount(discount,adjAmount){
        //     var calc = discountIntTaka(discount) + adjAmount;
        //     return parseFloat(calc);
        // }
        function AdjDiscount(charge,discount,adjAmount){
            var totalAfterDiscount = (charge*discountIntTaka(discount));
            var calc = totalAfterDiscount + adjAmount;
            return parseFloat(calc.toFixed(2));
        }
        
    })

})();
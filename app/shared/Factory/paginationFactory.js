angular.module('common')
    .factory('paginationFactory', ['config', paginationFactory])

function paginationFactory(config) {
    return {
        getItems: function (_totalPage, _currentPage, _offsetFromCurrentPage) {
            var paginationList = [];

            var item = [];
            for (var i = 1; i <= _totalPage; i++) {
                item.push(i);
            }

            var totalPage = item;

            var currentPage = parseInt(_currentPage);
            var offsetFromCurrentPage = _offsetFromCurrentPage || 2;
            //Process
            var pageRange = offsetFromCurrentPage * 2 + 1;
            var leftOffset = currentPage - offsetFromCurrentPage;
            var rightOffset = currentPage + offsetFromCurrentPage;
            var firstPage = parseInt(totalPage[0]);
            var lastPage = parseInt(totalPage[totalPage.length - 1]);
            //debugger;

            //console.log(totalPage.slice((currentPage - pageRange),(currentPage - pageRange) + 5))

            if (currentPage == firstPage) {
                paginationList = totalPage.slice(0, pageRange);
            } else {
                if (currentPage == lastPage) {
                    paginationList = totalPage.slice((currentPage - pageRange), (currentPage - pageRange) + 5);
                } else {
                    if (currentPage == 2) {
                        leftOffset = 0;
                    } else {
                        leftOffset = leftOffset - 1
                    }
                    paginationList = totalPage.slice(leftOffset, rightOffset);
                }
            }

            return paginationList;
        }
    }

}
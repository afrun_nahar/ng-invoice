angular.module('common')
    .factory('authFactory', ['tokenFactory', authFactory])

function authFactory(tokenFactory) {
    return {
        isAuthenticated : function () {
            var tken = tokenFactory.getToken();
            if (tken) {
                return true;
            } else {
                return false;
            }
        }
        , isAuthrozied : function(authItems){
            var isAuth = false;
            //get from localstorage
            if (authItems) {
                if (authItems.user) {
                    var user = tokenFactory.getUser()
                    if (Array.isArray(authItems.user)) {
                        if (authItems.user.indexOf(user) != -1) {
                            isAuth = true;
                        }
                    } else {
                        isAuth = authItems.user == user;
                    }
                }
            } else {
                isAuth = true;
            }
            
            return isAuth;
        }

    }


}
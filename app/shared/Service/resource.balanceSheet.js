angular.module('dataresource')
.service('balanceSheet', function($http, config){

    var bs = this;

    bs.GetAsset = function(){
        return getFinancialTransaction("A");
    };

    bs.GetLiability = function(){
        return getFinancialTransaction("L");
    };

    function getFinancialTransaction(glCategory){
        return $http.get(config.apiAccountsUrl + "balanceSheets/fiscalYear/FY2017-18/glCategory/" + glCategory);
    }

    return bs;


});
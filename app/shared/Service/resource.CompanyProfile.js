angular.module('dataresource')
.service('resourceCompanyProfile', ['$http', 'config', function($http, config){
    var cp = this;
    cp.get = function(){
        return $http.get(config.apiUrl + "companyprofiles");
    };

}]);
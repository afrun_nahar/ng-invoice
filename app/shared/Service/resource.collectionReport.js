angular.module('dataresource')
    .service('resourceCollecectionReport', ['$http', 'config', '$filter', function($http, config, $filter){
        var cr = this;
        cr.GetDateRange = function(fromDate, toDate, pageNumber, pageSize){
            var stringPath = "fromDate/"+fromDate+"/toDate/"+toDate+"/pageNumber/"+pageNumber+"/pageSize/"+ pageSize;
            return getData(stringPath)
                // .then(function(response){
                //     // var dta = converDateFromList(response.data);
                //     // return groupBy(dta, 'createdDate');
                //     return response.data;
                // })
        };

        cr.GetByDocotr = function(fromDate, toDate, doctor, pageNumber, pageSize){
            var stringPath = "doctor/"+ doctor + "/" + "fromDate/"+fromDate+"/toDate/"+toDate+"/pageNumber/"+pageNumber+"/pageSize/"+ pageSize;
            return getData(stringPath);
        };

        cr.GetByReferralAndDoctor = function(referral, doctor, fromDate, toDate, pageNumber, pageSize){
            var stringPath = "referral/"+referral+"/doctor/"+ doctor + "/" + "fromDate/"+fromDate+"/toDate/"+toDate+"/pageNumber/"+pageNumber+"/pageSize/"+ pageSize;
            return getData(stringPath);
        };

        cr.GetByUser = function(fromDate, toDate, user, pageNumber, pageSize){
            var stringPath = "user/"+ user + "/" + "fromDate/"+fromDate+"/toDate/"+toDate+"/pageNumber/"+pageNumber+"/pageSize/"+ pageSize;
            return getData(stringPath);
        };

        cr.GetByHospital = function(fromDate, toDate, referral, pageNumber, pageSize){
            var stringPath = "referral/"+ referral + "/" + "fromDate/"+fromDate+"/toDate/"+toDate+"/pageNumber/"+pageNumber+"/pageSize/"+ pageSize;
            return getData(stringPath);
        };
		
		 cr.GetByMarketingReferral = function(marketingReferral,fromDate, toDate, pageNumber, pageSize){
            var stringPath = "marketingReferral/"+ marketingReferral + "/" + "fromDate/"+fromDate+"/toDate/"+toDate+"/pageNumber/"+pageNumber+"/pageSize/"+ pageSize;
            return getData(stringPath);
        };

        function getData(stringPath, pageSize, pageNumber){
            return $http.get(config.apiAccountsUrl + "cashCollections/" + stringPath);
            // .then(function (result) {
            //     $scope.ReportList = result.data;
            // })
        }

        function convertToYMD(date){
            return $filter('date')(date, 'yyyy-MM-dd')
        }

        function converDateFromList(items){
            for (var i = 0; i < items.length; i++) {
                var element = items[i];
                element.createdDate = convertToYMD(element.createdDate)
            }
            return items;
        }

        function groupBy(xs, key) {
            return xs.reduce(function(rv, x) {
              (rv[x[key]] = rv[x[key]] || []).push(x);
              return rv;
            }, {});
          };
        return cr;

    }]);
angular.module('customFilter')
.filter("groupBy", function () {
    return function (items, item) {
        if (items) {
        //function groupBy(xs, key) {
            return items.reduce(function(rv, x) {
              (rv[x[item]] = rv[x[item]] || []).push(x);
              return rv;
            }, {});
          //};
        }
    }
})

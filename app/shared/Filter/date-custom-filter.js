/**
 * Created by DIU on 8/22/2017.
 */
angular.module('customFilter', [])
    .filter("dateInstance", function () {
        return function (items, item) {

            if (Array.isArray(item)) {
                    multiplecolumntotal(items, item)

            } else {

                if(typeof items[item] === "string"){
                    items[item] = new Date(items[item]);
                }

            }

            function multiplecolumntotal(items, columnlist) {

                if (columnlist.length > 0) {
                    for (var i = 0; i < columnlist.length; i++) {
                        items[item[i]] = new Date(items[item[i]]);
                    }
                }
            }

            return items;
        }
    })

    
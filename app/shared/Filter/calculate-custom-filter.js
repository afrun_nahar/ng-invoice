/**
 * Created by DIU on 8/22/2017.
 */
angular.module('customFilter')
    .filter("calculateBy", function () {
        return function (items, item) {
            var total = 0;
            

            //if item exist
            if(items){
                //if item has objects
                if(items.length > 0){
                    //repeat all items
                    for (var r = 0; r < items.length; r++) {
                        //if column name is in an array
                        if(Array.isArray(item)){
                            if(item.length > 0){
                                var rowTotal = 0;
                                for (var c = 0; c < item.length; c++) {
                                    if(rowTotal == 0){
                                        rowTotal = items[r][item[c]];
                                    } else {
                                        rowTotal *= items[r][item[c]];
                                    }
                                }
                                total += rowTotal;
                            }
                        } else {
                            total += isInt(items[r][item]) ? parseInt(items[r][item]) : parseFloat(items[r][item]);
                        }
                    }
                }
            }
            function isInt(n) {
                return n % 1 === 0;
             }

            return total;
        }
    });
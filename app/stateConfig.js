/**
 * Created by spider on 12/19/2017.
 */
angular.module('dash.ui.state', ['oc.lazyLoad'])


    .config(function ($stateProvider, $urlRouterProvider, $ocLazyLoadProvider) {
        $ocLazyLoadProvider.config({
            debug: false,

            modules: [
                {
                    name: 'companyHeader',
                    files: [
                        'shared/Service/resource.CompanyProfile.js'
                        , 'shared/Directive/companyHeader.directive.js'
                    ],
                    serie: true
                },
                {
                    name: 'bootstrap-table',
                    files: [
                        'assets/css/bootstrap-table.css'
                    ],
                    serie: true
                },
                {
                    name: 'print',
                    files: [
                        'assets/css/print.css'
                    ],
                    serie: true
                },
                {
                    name: 'inword',
                    files: [
                        'shared/Filter/inword-custom-filter.js'
                    ],
                    serie: true
                },
                {
                    name: 'report',
                    files: [
                        'assets/css/bootstrap-table.css',
                        'assets/css/print.css',
                        'shared/Filter/inword-custom-filter.js'
                    ],
                    serie: true
                }
                ,{
                    name: 'calculation',
                    files: [
                        'shared/Filter/calculate-custom-filter.js'
                    ],
                    serie: true
                }
                ,{
                    name: 'customDate',
                    files: [
                        'shared/Factory/customDateFactory.js'
                    ],
                    serie: true
                }
            ]

        });

        $stateProvider.state("root", {
            url: "",
            abstract: true,
            templateUrl: "component/masterPage.html",
            controllerAs: 'master',
            controller: 'masterPageController'
            , resolve: {
                loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                    // you can lazy load files for an existing module
                    return $ocLazyLoad.load([
                        //'shared/Filter/calculate-custom-filter.js',
                        'component/masterPageController.js'
                    ]);
                }]
            },
            authenticate: true
        })

            .state("root.login", {
                url: "/login",

                views: {
                    'homeView': {
                        controller: 'loginCtrl',
                        templateUrl: "component/login/login.html"
                    }
                }
                , resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load([
                            //'loginModule',
                            // 'localStorage',
                            // 'confirmModule',
                            'shared/Factory/notifyFactory.js',
                            'component/login/loginController.js'])

                    }]
                }
            })


            .state("root.dashboard", {
                url: "/dashboard",
                cache: false,
                abstract:true,
                views: {
                    'homeView': {
                        controller: 'dashboardMasterCtrl',
                        controllerAs: 'dashMaster',
                        templateUrl: "component/dashboard/dashboardMaster.html"
                    }
                }
                , resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load([
                                // 'localStorage',
                                // 'loginModule',
                                // 'roleFactory',
                                'component/dashboard/dashboardMasterController.js'
                            ]
                        );
                    }]
                },
                authenticate: true,
                authorization: {
                    user: ['administrator']
                }
            })

            .state("root.dashboard.home", {
                url: "/home",

                views: {
                    'dashboardHomeContent': {
                        controller: 'homeCtrl',
                        templateUrl: "component/dashboard/home/home.html"
                    }
                }
                , resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load(['component/dashboard/home/homeController.js'])
                    }]
                },
                authenticate: true,
                authorization: {
                    user: ['demo', 'administrator']
                }
            })
            .state("root.forgotPassword", {
                url: "/forgotPassword",

                views: {
                    'homeView': {
                        controller: 'forgotPassCtrl',
                        templateUrl: "component/forgotPassword/forgotPass.html"
                    }
                }
                , resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load(['component/forgotPassword/forgotPassController.js'])
                    }]
                }
            })
            .state("root.resetPassword", {
                url: "/resetPassword",

                views: {
                    'homeView': {
                        controller: 'resetPassCtrl',
                        templateUrl: "component/forgotPassword/resetPass.html"
                    }
                }
                , resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load(['component/forgotPassword/resetPassController.js'])
                    }]
                },
                authenticate: true,
                authorization: {
                    user: ['administrator']
                }
            })
            .state("root.dashboard.changePassword", {
                url: "/changePassword",

                views: {
                    'dashboardHomeContent': {
                        controller: 'changePassCtrl',
                        templateUrl: "component/dashboard/changePassword/changePassword.html"
                    }
                }
                , resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load(['component/dashboard/changePassword/changePassController.js'])
                    }]
                },
                authenticate: true
                
            })
        .state("root.dashboard.labGroup", {
                url: "/labGroup",

                views: {
                    'dashboardHomeContent': {
                        controller: 'labGroupCtrl',
                        controllerAs: 'labVm',
                        templateUrl: "component/dashboard/lab/labGroup/labGroup.html"
                    }
                }
                , resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load([
                            'component/dashboard/lab/labGroup/labGroupController.js'])

                    }]
                },
                authenticate: true,
                authorization: {
                    user: ['administrator']
                }
            })
            .state("root.dashboard.createLabGroup", {
                url: "/createLabGroup/:id",

                views: {
                    'dashboardHomeContent': {
                        controller: 'createGrpCtrl',
                        templateUrl: "component/dashboard/lab/labGroup/createGrp.html"
                    }
                }
                , resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load([
                            'component/dashboard/lab/labGroup/createGrpController.js'])

                    }]
                },
                authenticate: true,
                authorization: {
                    user: ['administrator']
                }
            })
            .state("root.dashboard.labInvestigation", {
                url: "/labInvestigation",

                views: {
                    'dashboardHomeContent': {
                        controller: 'InvestigationCtrl',
                        templateUrl: "component/dashboard/lab/labInvestigation/labInvestigation.html"
                    }
                }
                , resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load([
                            'component/dashboard/lab/labInvestigation/labInvestigationController.js'])

                    }]
                },
                authenticate: true,
                authorization: {
                    user: ['administrator']
                }
            })
            .state("root.dashboard.addLabInvestigation", {
                url: "/addLabInvestigation/:id",

                views: {
                    'dashboardHomeContent': {
                        controller: 'addInvestigationCtrl',
                        templateUrl: "component/dashboard/lab/labInvestigation/createInvestigation.html"
                    }
                }
                , resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load([
                            'component/dashboard/lab/labInvestigation/addInvestigationController.js'
                        ]);

                    }]
                },
                authenticate: true,
                authorization: {
                    user: ['administrator']
                }
            })
            .state("root.dashboard.createLabInvoice", {
                url: "/createLabInvoice/:id",

                views: {
                    'dashboardHomeContent': {
                        controller: 'invoiceCtrl',
                        templateUrl: "component/dashboard/lab/labInvoice/invoiceAdd.html"
                    }
                }
                , resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load([
                            'shared/Filter/calculate-custom-filter.js',
                            'shared/Directive/Dir.Investigation.row.js',
                            'shared/Factory/calculationLabFactory.js',
                            'component/dashboard/lab/labInvoice/invoiceAddController.js'
                        ])

                    }]
                },
                authenticate: true,
                authorization: {
                    user: ['administrator']
                }
            })
            .state("root.dashboard.labInvoice", {
                url: "/labInvoice",

                views: {
                    'dashboardHomeContent': {
                        controller: 'invoiceListCtrl',
                        templateUrl: "component/dashboard/lab/labInvoice/invoiceList.html"
                    }
                }
                , resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load([
                            //'stringPath',
                            'component/dashboard/lab/labInvoice/invoiceListController.js'
                        ])

                    }]
                },
                authenticate: true,
                authorization: {
                    user: ['administrator']
                }
            })
            .state("root.dashboard.invoiceDueCollection", {
                url: "/invoiceDueCollection/:id",

                views: {
                    'dashboardHomeContent': {
                        controller: 'invoiceDueCtrl',
                        templateUrl: "component/dashboard/lab/labInvoice/dueCollection/invoiceDue.html"
                    }
                }
                , resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load([
                            'component/dashboard/lab/labInvoice/dueCollection/invoiceDueController.js'])

                    }]
                },
                authenticate: true,
                authorization: {
                    user: ['administrator']
                }
            })
            .state("root.dashboard.dueCollectionList", {
                url: "/dueCollectionList",
                views: {
                    'dashboardHomeContent': {
                        controller: 'dueCollectionListCtrl',
                        templateUrl: "component/dashboard/lab/labInvoice/dueCollectionList/dueCollectionList.html"
                    }
                }
                , resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load([
                            'companyHeader',
                            'calculation',
                            'bootstrap-table',
                            'print',
                            'customDate',
                            'component/dashboard/lab/labInvoice/dueCollectionList/dueCollectionListController.js'])

                    }]
                },
                authenticate: true,
                authorization: {
                    user: ['administrator']
                }
            })
            .state("root.dashboard.printDueCollection", {
                url: "/printDueCollection/:id",

                views: {
                    'dashboardHomeContent': {
                        controller: 'printDueCollectionCtrl',
                        templateUrl: "component/dashboard/lab/labInvoice/dueCollectionList/printDueCollection.html"
                    }
                }
                , resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load([
                            'companyHeader',
                            'bootstrap-table',
                            'print',
                            'inword',
                            'component/dashboard/lab/labInvoice/dueCollectionList/printDueCollectionController.js'
                        ])

                    }]
                },
                authenticate: true,
                authorization: {
                    user: ['administrator']
                }
            })
            .state("root.dashboard.monthwiseDueReport", {
                url: "/monthwiseDueReport/:id",

                views: {
                    'dashboardHomeContent': {
                        controller: 'monthReportCtrl',
                        templateUrl: "component/dashboard/monthwiseDueReport/monthReport.html"
                    }
                }
                , resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load([
                            'calculation',
                            'customDate',
                            'companyHeader',
                            'bootstrap-table',
                            'print',
                            'inword',
                            'component/dashboard/monthwiseDueReport/monthReportController.js'
                        ])

                    }]
                },
                authenticate: true,
                authorization: {
                    user: ['administrator']
                }
            })
            .state("root.dashboard.createBundleDueCollection", {
                url: "/createBundleDueCollection",

                views: {
                    'dashboardHomeContent': {
                        controller: 'bundleDueCollectionCtrl',
                        templateUrl: "component/dashboard/bundleDue/CreateBundleDueCollection.html"
                    }
                }
                , resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load([
                            'calculation',
                            'bootstrap-table',
                            'print',
                            'shared/Factory/notifyFactory.js',
                            'component/dashboard//bundleDue/createBundleDueController.js'
                        ])

                    }]
                },
                authenticate: true,
                authorization: {
                    user: ['administrator']
                }
            })
            .state("root.dashboard.printInvoice", {
                url: "/printInvoice/:id",

                views: {
                    'dashboardHomeContent': {
                        controller: 'printInvoiceCtrl',
                        templateUrl: "component/dashboard/lab/labInvoice/printInvoice.html"
                    }
                }
                , resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load([
                            'companyHeader',
                            'bootstrap-table', 'print', 'inword',
                            'component/dashboard/lab/labInvoice/printInvoiceController.js'
                        ])

                    }]
                },
                authenticate: true,
                authorization: {
                    user: ['administrator']
                }
            })

            .state("root.dashboard.labReportList", {
                url: "/labReportList",
                views: {
                    'dashboardHomeContent': {
                        controller: 'labReportListCtrl',
                        templateUrl: "component/dashboard/lab/labReport/labReport.html"
                    }
                }
                , resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load([
                            'bootstrap-table',
                            'component/dashboard/lab/labReport/labReportController.js'
                        ])

                    }]
                },
                authenticate: true,
                authorization: {
                    user: ['administrator']
                }
            })
            .state("root.dashboard.printReport", {
                url: "/printReport/:receiptId/:reportId",

                views: {
                    'dashboardHomeContent': {
                        controller: 'reportPrintCtrl',
                        templateUrl: "component/dashboard/lab/labReport/printReport.html"
                    }
                }
                , resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load([
                            'bootstrap-table',
                            'print',
                            'companyHeader',
                            'component/dashboard/lab/labReport/printReportController.js'
                        ])
                        // 'print',
                    }]
                },
                authenticate: true,
                authorization: {
                    user: ['administrator']
                }
            })

            .state("root.dashboard.accounts", {
                url: "/accounts",
                cache: false,
                abstract:true,
                views: {
                    'dashboardHomeContent': {
                        controller: 'accountMasterCtrl',
                        templateUrl: "component/dashboard/accounts/accountMaster.html"
                    }
                }
                , resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load([
                                'component/dashboard/accounts/accountMasterController.js'
                            ]
                        );
                    }]
                },
                authenticate: true,
                authorization: {
                    user: ['administrator']
                }

            })
            .state("root.dashboard.accounts.homeAccounts", {
                url: "/homeAccounts",

                views: {
                    'homeAccountMaster': {
                        controller: 'accountHomeCtrl',
                        templateUrl: "component/dashboard/accounts/home/homeAccount.html"
                    }
                }
                , resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load([
                            'component/dashboard/accounts/home/homeAccountController.js'])

                    }]
                },
                authenticate: true,
                authorization: {
                    user: ['administrator']
                }
            })
            .state("root.dashboard.accounts.journalVoucher", {
                url: "/journalVoucher",

                views: {
                    'homeAccountMaster': {
                        controller: 'journalVoucherCtrl',
                        templateUrl: "component/dashboard/accounts/voucher/JournalVoucher.html"
                    }
                }
                , resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load([
                            'shared/Factory/notifyFactory.js',
                            'calculation',
                            'shared/Factory/voucherFactory.js',
                            'bootstrap-table',
                            'component/dashboard/accounts/voucher/journalVoucherController.js'])

                    }]
                },
                authenticate: true,
                authorization: {
                    user: ['administrator']
                }
            })
            .state("root.dashboard.accounts.paymentVoucher", {
                url: "/paymentVoucher",

                views: {
                    'homeAccountMaster': {
                        controller: 'paymentVoucherCtrl',
                        templateUrl: "component/dashboard/accounts/voucher/paymentVoucher.html"
                    }
                }
                , resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load([
                            'calculation',
                            'shared/Factory/voucherFactory.js',
                            'shared/Filter/calculate-custom-filter.js',
                            'shared/Factory/notifyFactory.js',
                            'bootstrap-table',
                            'component/dashboard/accounts/voucher/paymentVoucherController.js'])

                    }]
                },
                authenticate: true,
                authorization: {
                    user: ['administrator']
                }
            })
            .state("root.dashboard.accounts.incomeExpenditure", {
                url: "/incomeExpenditure",

                views: {
                    'homeAccountMaster': {
                        controller: 'incomeCtrl',
                        templateUrl: "component/dashboard/accounts/incomeExpenditure/income.html"
                    }
                }
                , resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load([
                            'print',
                            'companyHeader',
                            'calculation',
                            'bootstrap-table',
                            'component/dashboard/accounts/incomeExpenditure/incomeController.js'])

                    }]
                },
                authenticate: true,
                authorization: {
                    user: ['administrator']
                }
            })
            .state("root.dashboard.accounts.trailBalance", {
                url: "/trailBalance",

                views: {
                    'homeAccountMaster': {
                        controller: 'trailBalanceCtrl',
                        templateUrl: "component/dashboard/accounts/trialBalance/trailBalance.html"
                    }
                }
                , resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                        // you can lazy load files f
                        // or an existing module
                        return $ocLazyLoad.load([
                            'calculation',
                            'bootstrap-table',
                            'component/dashboard/accounts/trialBalance/trailBalanceController.js'])

                    }]
                },
                authenticate: true,
                authorization: {
                    user: ['administrator']
                }
            })
            .state("root.dashboard.accounts.balanceSheet", {
                url: "/balanceSheet",

                views: {
                    'homeAccountMaster': {
                        controller: 'balanceSheetCtrl',
                        templateUrl: "component/dashboard/accounts/balanceSheet/balanceSheet.html"
                    }
                }
                , resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                        // you can lazy load files f
                        // or an existing module
                        return $ocLazyLoad.load([
                            'shared/Service/resource.balanceSheet.js',
                            'calculation',
                            'bootstrap-table',
                            'component/dashboard/accounts/balanceSheet/balanceSheetController.js'])

                    }]
                },
                authenticate: true,
                authorization: {
                    user: ['administrator']
                }
            })
            .state("root.dashboard.accounts.voucherList", {
                url: "/voucherList/:fromDate/:toDate/:pageNumber/:pageSize",

                views: {
                    'homeAccountMaster': {
                        controller: 'voucherListCtrl',
                        templateUrl: "component/dashboard/accounts/voucher/voucherList.html"
                    }
                }
                , resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load([
                            //'bootstrap-table',
                            'shared/Factory/paginationFactory.js',
                            'component/dashboard/accounts/voucher/voucherListController.js'
                        ])

                    }]
                },
                authenticate: true,
                authorization: {
                    user: ['administrator']
                }
            })
            .state("root.dashboard.accounts.voucherPrint", {
                url: "/voucherPrint/:id/:type",
                views: {
                    'homeAccountMaster': {
                        controller: 'voucherPrintCtrl',
                        templateUrl: "component/dashboard/accounts/voucher/voucherPrint.html"
                    }
                }
                , resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load([
                            'bootstrap-table',
                            'companyHeader',
                            'calculation',
                            'shared/Factory/notifyFactory.js',
                            'shared/Factory/voucherFactory.js',
                            'print',
                            'component/dashboard/accounts/voucher/voucherPrintController.js'
                        ])

                    }]
                },
                authenticate: true,
                authorization: {
                    user: ['administrator']
                }
            })
            .state("root.dashboard.invoiceTemplate", {
                url: "/invoiceTemplate",
                views: {
                    'dashboardHomeContent': {
                        controller: 'invoiceTemplateCtrl',
                        templateUrl: "component/dashboard/lab/invoiceTemplate/invoiceTemplate.html"
                    }
                }
                , resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load([
                            'component/dashboard/lab/invoiceTemplate/invoiceTemplateController.js']);
                    }]
                },
                authenticate: true,
                authorization: {
                    user: ['administrator']
                }
            })
            .state("root.dashboard.createInvoiceTemplate", {
                url: "/createInvoiceTemplate/:id",

                views: {
                    'dashboardHomeContent': {
                        controller: 'createTmpltCtrl',
                        templateUrl: "component/dashboard/lab/invoiceTemplate/createTmplt.html"
                    }
                }
                , resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load([
                            'component/dashboard/lab/invoiceTemplate/createTmpltController.js']);

                    }]
                },
                authenticate: true,
                authorization: {
                    user: ['administrator']
                }
            })
            .state("root.dashboard.collectionReport", {
                url: "/collectionReport",

                views: {
                    'dashboardHomeContent': {
                        controller: 'collectionReportCtrl',
                        templateUrl: "component/dashboard/collectionReport/collectionReport.html"
                    }
                }
                , resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load([
                            'bootstrap-table',
                            'print',
                            'shared/Filter/calculate-custom-filter.js',
                            'companyHeader',
                            'shared/Service/resource.collectionReport.js',
                            'component/dashboard/collectionReport/collectionReportController.js'
                        ]);

                    }]
                },
                authenticate: true,
                authorization: {
                    user: ['administrator']
                }
            })
            .state("root.dashboard.collectionReportSummaraized", {
                url: "/collectionReportSummaraized",
                views: {
                    'dashboardHomeContent': {
                        controller: 'collectionReportSummaraizedCtrl',
                        templateUrl: "component/dashboard/collectionReport/collectionReport.summaraized.html"
                    }
                }
                , resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load([
                            'bootstrap-table',
                            'print',
                            'shared/Filter/calculate-custom-filter.js',
                            'shared/Service/resource.CompanyProfile.js',
                            'shared/Service/resource.collectionReport.js',
                            'component/dashboard/collectionReport/collectionReport.summaraized.js'
                        ]);

                    }]
                },
                authenticate: true,
                authorization: {
                    user: ['administrator']
                }
            })
            .state("root.dashboard.printCollectionReport", {
                url: "/printCollectionReport",

                views: {
                    'dashboardHomeContent': {
                        controller: 'collectionPrintCtrl',
                        templateUrl: "component/dashboard/collectionReport/collectionPrint.html"
                    }
                }
                , resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load([
                            'companyHeader',
                            'bootstrap-table','print',
                            'component/dashboard/collectionReport/collectionPrintController.js'])

                    }]
                },
                authenticate: true,
                authorization: {
                    user: ['administrator']
                }
            })
            .state("root.dashboard.reportDeliverySystem", {
                url: "/reportDeliverySystem/:status",

                views: {
                    'dashboardHomeContent': {
                        controller: 'reportDeliveryCtrl',
                        templateUrl: "component/dashboard/reportDelivery/reportDelivery.html"
                    }
                }
                , resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load([
                            'bootstrap-table',
                            'component/dashboard/reportDelivery/reportDeliveryController.js'])

                    }]
                },
                authenticate: true,
                authorization: {
                    user: ['administrator']
                }
            })

            .state("root.dashboard.authorisation", {
                url: "/authorisation",
                cache: false,
                abstract:true,
                views: {
                    'dashboardHomeContent': {
                        controller: 'authorizationMasterCtrl',
                        templateUrl: "component/dashboard/authorization/authorizationMaster.html"
                    }
                }
                , resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load([
                                'component/dashboard/authorization/authorizationMasterController.js'
                            ]
                        );
                    }]
                },
                authenticate: true,
                authorization: {
                    user: ['administrator']
                }

            })
            // .state("root.dashboard.authorisation.homeAuthorization", {
            //     url: "/homeAuthorization",
            //
            //
            //     views: {
            //         'homeAuthorizationMaster': {
            //             controller: 'homeAuthCtrl',
            //             templateUrl: "component/dashboard/authorization/home/homeAuth.html"
            //         }
            //     }
            //     , resolve: {
            //         loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
            //             // you can lazy load files for an existing module
            //             return $ocLazyLoad.load([
            //                 'component/dashboard/authorization/home/homeAuthController.js'])
            //
            //         }]
            //     }
            // })
            .state("root.dashboard.authorisation.roleUser", {
                url: "/roleUser",

                views: {
                    'homeAuthorizationMaster': {
                        controller: 'roleAuthCtrl',
                        templateUrl: "component/dashboard/authorization/role/role.html"
                    }
                }
                , resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load([
                            'bootstrap-table',
                            'component/dashboard/authorization/role/roleController.js'])

                    }]
                },
                authenticate: true,
                authorization: {
                    user: ['administrator']
                }
            })
            .state("root.dashboard.authorisation.userInfo", {
                url: "/userInfo",

                views: {
                    'homeAuthorizationMaster': {
                        controller: 'userInfoCtrl',
                        templateUrl: "component/dashboard/authorization/userInfo/userInfo.html"
                    }
                }
                , resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load([
                            'bootstrap-table',
                            'component/dashboard/authorization/userInfo/userInfoController.js'])

                    }]
                },
                authenticate: true,
                authorization: {
                    user: ['administrator']
                }
            })
            .state("root.dashboard.authorisation.createUserInfo", {
                url: "/createUserInfo/:id",

                views: {
                    'homeAuthorizationMaster': {
                        controller: 'addUserInfoCtrl',
                        templateUrl: "component/dashboard/authorization/userInfo/addUserinfo.html"
                    }
                }
                , resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load([
                            'bootstrap-table',
                            'component/dashboard/authorization/userInfo/addUserinfoController.js'])

                    }]
                },
                authenticate: true,
                authorization: {
                    user: ['administrator']
                }
            })

            .state("root.dashboard.authorisation.authUserRole", {
                url: "/authUserRole",

                views: {
                    'homeAuthorizationMaster': {
                        controller: 'authRoleUserCtrl',
                        templateUrl: "component/dashboard/authorization/authUserRole/authRoleUser.html"
                    }
                }
                , resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load([
                            'component/dashboard/authorization/authUserRole/authRoleUserController.js'])

                    }]
                },
                authenticate: true,
                authorization: {
                    user: ['administrator']
                }
            })
            .state("root.dashboard.authorisation.createAuthUserRole", {
                url: "/createAuthUserRole/:roleId",

                views: {
                    'homeAuthorizationMaster': {
                        controller: 'addAuthUserCtrl',
                        templateUrl: "component/dashboard/authorization/authUserRole/addAuthUser.html"
                    }
                }
                , resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load([
                            'component/dashboard/authorization/authUserRole/addAuthUserController.js'])

                    }]
                },
                authenticate: true,
                authorization: {
                    user: ['administrator']
                }
            })
            .state("root.forgotPass", {
                url: "/forgotPass/?:userName",

                views: {
                    'homeView': {
                        controller: 'forgotPassCtrl',
                        templateUrl: "component/forgotPassword/forgotPass.html"
                    }
                }
                , resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load([
                            'component/forgotPassword/forgotPassController.js'])

                    }]
                }
            })



        $urlRouterProvider.otherwise("/login")

    });


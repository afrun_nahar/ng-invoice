/**
 * Created by spider on 5/29/2017.
 */
$(document).ready(function(){

    $(".dropdown-button").dropdown();

    $('.button-collapse').sideNav({
            menuWidth: 300, // Default is 300
            edge: 'left', // Choose the horizontal origin
            closeOnClick: true, // Closes side-nav on <a> clicks, useful for Angular/Meteor
            draggable: true // Choose whether you can drag to open on touch screens
        }
    );
});


// Highcharts.chart('container', {
//
//         chart: {
//             type: 'gauge',
//             plotBackgroundColor: null,
//             plotBackgroundImage: null,
//             plotBorderWidth: 0,
//             plotShadow: false
//         },
//
//         title: {
//             text: 'Speedometer'
//         },
//
//         pane: {
//             startAngle: -150,
//             endAngle: 150,
//             background: [{
//                 backgroundColor: {
//                     linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
//                     stops: [
//                         [0, '#FFF'],
//                         [1, '#333']
//                     ]
//                 },
//                 borderWidth: 0,
//                 outerRadius: '109%'
//             }, {
//                 backgroundColor: {
//                     linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
//                     stops: [
//                         [0, '#333'],
//                         [1, '#FFF']
//                     ]
//                 },
//                 borderWidth: 1,
//                 outerRadius: '107%'
//             }, {
//                 // default background
//             }, {
//                 backgroundColor: '#DDD',
//                 borderWidth: 0,
//                 outerRadius: '105%',
//                 innerRadius: '103%'
//             }]
//         },
//
//         // the value axis
//         yAxis: {
//             min: 0,
//             max: 200,
//
//             minorTickInterval: 'auto',
//             minorTickWidth: 1,
//             minorTickLength: 10,
//             minorTickPosition: 'inside',
//             minorTickColor: '#666',
//
//             tickPixelInterval: 30,
//             tickWidth: 2,
//             tickPosition: 'inside',
//             tickLength: 10,
//             tickColor: '#666',
//             labels: {
//                 step: 2,
//                 rotation: 'auto'
//             },
//             title: {
//                 text: 'km/h'
//             },
//             plotBands: [{
//                 from: 0,
//                 to: 120,
//                 color: '#55BF3B' // green
//             }, {
//                 from: 120,
//                 to: 160,
//                 color: '#DDDF0D' // yellow
//             }, {
//                 from: 160,
//                 to: 200,
//                 color: '#DF5353' // red
//             }]
//         },
//
//         series: [{
//             name: 'Speed',
//             data: [80],
//             tooltip: {
//                 valueSuffix: ' km/h'
//             }
//         }]
//
//     },
// // Add some life
//     function (chart) {
//         if (!chart.renderer.forExport) {
//             setInterval(function () {
//                 var point = chart.series[0].points[0],
//                     newVal,
//                     inc = Math.round((Math.random() - 0.5) * 20);
//
//                 newVal = point.y + inc;
//                 if (newVal < 0 || newVal > 200) {
//                     newVal = point.y - inc;
//                 }
//
//                 point.update(newVal);
//
//             }, 3000);
//         }
//     });
//
// Highcharts.chart('cont', {
//     chart: {
//         type: 'column'
//     },
//     title: {
//         text: 'Browser market shares. January, 2015 to May, 2015'
//     },
//     subtitle: {
//         text: 'Click the columns to view versions. Source: <a href="http://netmarketshare.com">netmarketshare.com</a>.'
//     },
//     xAxis: {
//         type: 'category'
//     },
//     yAxis: {
//         title: {
//             text: 'Total percent market share'
//         }
//
//     },
//     legend: {
//         enabled: false
//     },
//     plotOptions: {
//         series: {
//             borderWidth: 0,
//             dataLabels: {
//                 enabled: true,
//                 format: '{point.y:.1f}%'
//             }
//         }
//     },
//
//     tooltip: {
//         headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
//         pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
//     },
//
//     series: [{
//         name: 'Brands',
//         colorByPoint: true,
//         data: [{
//             name: 'Microsoft Internet Explorer',
//             y: 56.33,
//             drilldown: 'Microsoft Internet Explorer'
//         }, {
//             name: 'Chrome',
//             y: 24.03,
//             drilldown: 'Chrome'
//         }, {
//             name: 'Firefox',
//             y: 10.38,
//             drilldown: 'Firefox'
//         }, {
//             name: 'Safari',
//             y: 4.77,
//             drilldown: 'Safari'
//         }, {
//             name: 'Opera',
//             y: 0.91,
//             drilldown: 'Opera'
//         }, {
//             name: 'Proprietary or Undetectable',
//             y: 0.2,
//             drilldown: null
//         }]
//     }],
//     drilldown: {
//         series: [{
//             name: 'Microsoft Internet Explorer',
//             id: 'Microsoft Internet Explorer',
//             data: [
//                 [
//                     'v11.0',
//                     24.13
//                 ],
//                 [
//                     'v8.0',
//                     17.2
//                 ],
//                 [
//                     'v9.0',
//                     8.11
//                 ],
//                 [
//                     'v10.0',
//                     5.33
//                 ],
//                 [
//                     'v6.0',
//                     1.06
//                 ],
//                 [
//                     'v7.0',
//                     0.5
//                 ]
//             ]
//         }, {
//             name: 'Chrome',
//             id: 'Chrome',
//             data: [
//                 [
//                     'v40.0',
//                     5
//                 ],
//                 [
//                     'v41.0',
//                     4.32
//                 ],
//                 [
//                     'v42.0',
//                     3.68
//                 ],
//                 [
//                     'v39.0',
//                     2.96
//                 ],
//                 [
//                     'v36.0',
//                     2.53
//                 ],
//                 [
//                     'v43.0',
//                     1.45
//                 ],
//                 [
//                     'v31.0',
//                     1.24
//                 ],
//                 [
//                     'v35.0',
//                     0.85
//                 ],
//                 [
//                     'v38.0',
//                     0.6
//                 ],
//                 [
//                     'v32.0',
//                     0.55
//                 ],
//                 [
//                     'v37.0',
//                     0.38
//                 ],
//                 [
//                     'v33.0',
//                     0.19
//                 ],
//                 [
//                     'v34.0',
//                     0.14
//                 ],
//                 [
//                     'v30.0',
//                     0.14
//                 ]
//             ]
//         }, {
//             name: 'Firefox',
//             id: 'Firefox',
//             data: [
//                 [
//                     'v35',
//                     2.76
//                 ],
//                 [
//                     'v36',
//                     2.32
//                 ],
//                 [
//                     'v37',
//                     2.31
//                 ],
//                 [
//                     'v34',
//                     1.27
//                 ],
//                 [
//                     'v38',
//                     1.02
//                 ],
//                 [
//                     'v31',
//                     0.33
//                 ],
//                 [
//                     'v33',
//                     0.22
//                 ],
//                 [
//                     'v32',
//                     0.15
//                 ]
//             ]
//         }, {
//             name: 'Safari',
//             id: 'Safari',
//             data: [
//                 [
//                     'v8.0',
//                     2.56
//                 ],
//                 [
//                     'v7.1',
//                     0.77
//                 ],
//                 [
//                     'v5.1',
//                     0.42
//                 ],
//                 [
//                     'v5.0',
//                     0.3
//                 ],
//                 [
//                     'v6.1',
//                     0.29
//                 ],
//                 [
//                     'v7.0',
//                     0.26
//                 ],
//                 [
//                     'v6.2',
//                     0.17
//                 ]
//             ]
//         }, {
//             name: 'Opera',
//             id: 'Opera',
//             data: [
//                 [
//                     'v12.x',
//                     0.34
//                 ],
//                 [
//                     'v28',
//                     0.24
//                 ],
//                 [
//                     'v27',
//                     0.17
//                 ],
//                 [
//                     'v29',
//                     0.16
//                 ]
//             ]
//         }]
//     }
// });
#API Refrance
###BaseURL: http://192.168.10.124/billbudget.api.app/api/
###Cost Center
####Models:
````
    [Required, MinLength(3), MaxLength(250)]
    public string CostCenterName { get; set; }
    public string Location { get; set; }
````
####Query:
1. api/costCenters
2. api/costCenters/65464654646456
3. api/costCenters/name/tr
4. api/costCenters/status/truefalse
5. api/costCenters/status/truefalse/pageSize/5/pageNumber/10
####Post:
api/costCenters

###JOBS
####Models:
````
    [Required]
    public string CostCenterId { get; set; }
    [Required,MinLength(3),MaxLength(250)]
    public string JobName { get; set; }
    public string JobDescription { get; set; }
    public bool? IsActive { get; set; }
    [Required]
    public DateTime StartedDate { get; set; }
    [Required]
    public DateTime EstimetedEndDate { get; set; }
    public DateTime? ActualEndDate { get; set; }
    public string ReferenceNo { get; set; }
    public string CreatedIP { get; set; }
    public string UpdatedIP { get; set; }
    public int? UpdatedBy { get; set; }
    public DateTime? UpdatedDate { get; set; }
    [Required]
    public string AssignTo { get; set; }
    public decimal? TotalBudget { get; set; }
    public decimal? TotalAdvance { get; set; }
    public decimal? TotalBill { get; set; }
    public decimal? TotalAdjust { get; set; }
    public string Note { get; set; }
    public string JobSupervisorId { get; set; }
````  
####Query:
1. api/jobs
2. api/jobs/65464654646456
3. api/jobs/name/tr
4. api/jobs/status/truefalse
5. api/jobs/costCenter/4654654
####Post:
api/jobs


###BUDGET
####Models:
````
    [Required, MinLength(3), MaxLength(250)]
    public string BudgetName { get; set; }
    public string CreatedIP { get; set; }
    public string UpdatedFromIP { get; set; }
    public int? UpdatedBy { get; set; }
    public DateTime? UpdatedDate { get; set; }
    public bool? Active { get; set; }
    [Required]
    public  string AssignTo { get; set; }
    public string ForwardTo { get; set; }
    public string ApplicationStatus { get; set; }
    public virtual IEnumerable<BudgetSubViewModel> BudgetSub { get; set; }
    [Required]
    public string JobId { get; set; }
    public string JobName { get; set; }
    public string CostCenterId { get; set; }
    public string CostCenterName { get; set; }
    public string CostCenterLocation { get; set; }
    public DateTime CreatedOn { get; set; }
    public string CreatedBy { get; set; }
````  
####Query:
1. api/budgets
2. api/budgets/65464654646456
3. api/budgets/name/tr
4. api/budgets/status/truefalse
5. api/budgets/costCenter/4654654
####Post:
api/budgets

###BUDGET-SUB
####Models:
````
    public string BudgetMainId { get; set; }
    [Required, MinLength(3), MaxLength(250)]
    public string ItemName { get; set; }
    [Required]
    public decimal Quantity { get; set; }
    [Required]
    public decimal UnitPrice { get; set; }
    public int? VendorId { get; set; }
    public string WorkOrderNo { get; set; }
    public string QoutationNo { get; set; }
    public decimal? BudgetAmount { get; set; }
    public decimal? BudgetApproveAmount { get; set; }
    public string CreatedIP { get; set; }
    public string UpdatedFromIP { get; set; }
    public int? UpdatedBy { get; set; }
    public DateTime? UpdatedDate { get; set; }
    public bool? IsRegisterVendor { get; set; }
    public string VendorName { get; set; }
    public string MeasurementUnit { get; set; }
    public string MeasurementUnitId { get; set; }
    public string CreatedBy { get; set; }
    public DateTime CreatedOn { get; set; }
````  
####Query:
1. api/budgets
2. api/budgets/65464654646456
3. api/budgets/costCenter/234324
4. api/budgets/assignTo/234234
####Post:
api/budgets

###WORK ORDERS
####Models:
````
    [Required]
    public string JobId { get; set; }
    public string WorkOrderNumber { get; set; }
    [Required,MinLength(3),MaxLength(250)]
    public string WorkOrderName { get; set; }
    public string Note { get; set; }
    public int? VendoreId { get; set; }
    public string VendoreName { get; set; }
    public string VendoreDetail { get; set; }
    public decimal? TotalAmount { get; set; }
    public decimal? DiscountAmount { get; set; }
    public string TermsCondition { get; set; }
    public DateTime? DelivaryDateTime { get; set; }
    public string DelivaryPlace { get; set; }
    public string ContactPerson { get; set; }
    public string CreatedIP { get; set; }
    public DateTime? WorkOrderDate { get; set; }
````  
####Query:
api/WorkOrders/job/234234
api/WorkOrders/234234
api/WorkOrders/keyvalue/job/3423423
####Post:
api/WorkOrders
####Delete
main item delete: api/workOrders/342342344
sub item delete: api/workOrders/sub/342342344

###VOUCHER
####Post
api/voucher

````
    public string Narration { get; set; }
    [Required, MaxLength(10)]
    public string PrjCode { get; set; }
    [Required]
    public string VoucherType { get; set; }
    [Required]
    public string ReferenceId { get; set; }
    [Required]
    public string ApprovalPurposeId { get; set; }
    [Required]
    public List<VoucherSubCreatingDto> VoucherDetails { get; set; }
````

````
    public string DeptCode { get; set; }
    [Required, MaxLength(20)]
    public string Ac_Code { get; set; }
    [Required, Range(0, 10000000)]
    public decimal Dr_amount { get; set; }
    [Required, Range(0, 10000000)]
    public decimal Cr_amount { get; set; }
    [MaxLength(15)]
    public string Chq_no { get; set; }
    [MaxLength(15)]
    public string Bill_no { get; set; }
    [MaxLength(50)]
    public string Inv_no { get; set; }
    [MaxLength(15)]
    public string MR_no { get; set; }
    [MaxLength(36)]
    public string Job_No { get; set; }
    [MaxLength(100)]
    public string Description { get; set; }
    [Required, Range(1, 1000)]
    public int AccCode { get; set; }
````


/**
 * Created by User on 5/16/2017.
 */
var gulp = require('gulp');
var htmlmin = require('gulp-htmlmin');
var htmlreplace = require('gulp-html-replace');
//var minifyjs = require('gulp-js-minify');
var minifyCSS = require('gulp-minify-css');
var uglify = require('gulp-uglify');
var rename = require("gulp-rename");
var sourcemaps = require('gulp-sourcemaps');
var replace = require('gulp-replace');
//var chmod = require('gulp-chmod');
var gutil = require('gulp-util');
const errorHandler = require('gulp-error-handle');
var sass = require('gulp-sass');
var gulpRemoveHtml = require('gulp-remove-html');
var gulpif = require('gulp-if');
var ngAnnotate = require('gulp-ng-annotate');

gulp.task('sass', function () {
    return gulp.src('./app/assets/bootstrapSass/bootstrap2.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./app/assets/css'));
});

// gulp.task('sass:watch', function () {
//     gulp.watch('./app/assets/sass/**/*.scss', ['sass']);
// });

//npm install --save-dev gulp gulp-htmlmin gulp-minify-css gulp-uglify gulp-rename gulp-sourcemaps gulp-replace

var _destination = 'build';

function getListOfLibrary(_basFolder) {
    return [
        _basFolder + "/js/angular.min.js",
        _basFolder + "/js/angular-messages.min.js",
        _basFolder + "/js/angular-animate.min.js",
        _basFolder + "/js/angular-aria.min.js",
        _basFolder + "/js/angular-material.min.js",
        _basFolder + "/js/ocLazyLoad.min.js",
        _basFolder + "/js/angular-filter.min.js",
        _basFolder + "/js/ui-router.min.js",
        _basFolder + "/js/moment.min.js",
    ];
}

var tasks = {
    htmlMinifyIndex: {name: 'html-minify-index', path: 'app/index.html'},
    htmlMinifyAll: {name: 'html-minify-all', path: 'app/component/**/**/**/*.html'},
    htmlMinifyTemplate: {name: 'html-minify-tmpl', path: 'app/shared/template/*.html'},
    transferJsLibrary:  {name: 'transfer-js-lib', path: getListOfLibrary("app/assets")},
    appJs:  {name: 'js-minify-app', path: 'app/*.js'},
    jsMinifyCtrl: {name: 'js-minify-ctrl', path: 'app/component/**/**/**/*.js'},
    jsMinifyShared:  {name: 'js-minify-sheared', path: 'app/shared/**/*.js'},
    imageTransferShared: {name: 'image-transfer-shared', path: 'app/shared/Image/*'},
    imageTransferAsset: {name: 'image-transfer-asset', path: 'app/assets/image/*'},
    iconTransferAsset: {name: 'icon-transfer-asset', path: 'app/assets/fonts/**/*'},
    minifyCssLib: {name: 'minify-css-lib', path: 'app/assets/css/*.css'}
}

gulp.task('an-test', function(){
    gulp.src([
        'app/shared/Factory/calculationLabFactory.js'
    ])
        .pipe(errorHandler())
        .pipe(ngAnnotate())
        .pipe(sourcemaps.init({ includeContent: false }))
        .pipe(rename({
            extname: ".min.js"
        }))
        .pipe(uglify({
            //outSourceMap: true,
            //report: 'min',
            compress: {
                drop_console: true
            },
            mangle: true
        }))
        .pipe(sourcemaps.write({addComment: false}))
        //.on('error', function (err) { gutil.log(gutil.colors.red('[Error]'), err.toString()); })
        .pipe(gulp.dest(_destination + '/component'));
});


gulp.task(tasks.htmlMinifyIndex.name, function() {
    return gulp.src(tasks.htmlMinifyIndex.path)
        .pipe(htmlreplace({ 'css':
            [
                'assets/css/angular-material.min.css',
                'assets/css/style.min.css'
            ]
            , 'jslibrary': getListOfLibrary("assets")
            , 'js': [
                
                "app.min.js",
                "stateConfig.min.js",
                "logOutController.min.js",
                "shared/Factory/httpRequestInterceptor.min.js",
                "shared/Factory/tockenFactory.min.js"
            ]
        }))
        .pipe(errorHandler())
        .pipe(htmlmin({
                collapseWhitespace: true
                , removeComments: true
                //, removeEmptyElements: true
                //, removeOptionalTags: true
                //, removeEmptyAttributes: true
                //, removeTagWhitespace: true
            }
        ))
        .pipe(gulp.dest(_destination));
});

gulp.task(tasks.htmlMinifyAll.name, function() {
    return gulp.src([tasks.htmlMinifyAll.path])
        .pipe(errorHandler())
        .pipe(sourcemaps.init())
        //.pipe(gulpif(_destination === 'build', gulpRemoveHtml()))
        .pipe(htmlmin({
                collapseWhitespace: true
                , removeComments: true
                , minifyCSS: minifyCSS()//Minify all inner css from html
            }
        ))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(_destination + '/component'));
});

gulp.task(tasks.htmlMinifyTemplate.name, function() {
    return gulp.src([tasks.htmlMinifyTemplate.path])
        .pipe(errorHandler())
        .pipe(gulpif(_destination === 'build', gulpRemoveHtml()))
        .pipe(sourcemaps.init())
        .pipe(htmlmin({
            collapseWhitespace: true
            , removeComments: true
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(_destination + '/shared/template'));
});

gulp.task('html-minify', [ tasks.htmlMinifyIndex.name, tasks.htmlMinifyAll.name, tasks.htmlMinifyTemplate.name ] );

//Minify Javascript files
gulp.task(tasks.transferJsLibrary.name, function(){
    gulp.src(tasks.transferJsLibrary.path)
    .pipe(errorHandler())
    .pipe(gulp.dest(_destination + '/assets/js'));
});

gulp.task(tasks.appJs.name, function(){
    gulp.src([tasks.appJs.path])
        .pipe(errorHandler())
        .pipe(ngAnnotate())
        .pipe(replace('.js', '.min.js'))
        .pipe(replace('.css', '.min.css'))
        .pipe(rename({
            extname: ".min.js"
        }))
        .pipe(uglify({
            //outSourceMap: true,
            //report: 'min',
            compress: {
                drop_console: true
            },
            mangle: true
        }))
        //.on('error', function (err) { gutil.log(gutil.colors.red('[Error]'), err.toString()); })
        .pipe(gulp.dest(_destination));
});

// gulp.task('replaceModuleName', function(){
//     gulp.src(['app/component/**/**/*.js'])
//         .pipe(replace('ecure', 'BABA'))
//
//         .pipe(gulp.dest('app/component/**/**/*.js'));
// });




gulp.task(tasks.jsMinifyCtrl.name, function(){
    gulp.src([
        tasks.jsMinifyCtrl.path
    ])
        .pipe(errorHandler())
        .pipe(ngAnnotate())
        .pipe(sourcemaps.init({ includeContent: false }))
        .pipe(rename({
            extname: ".min.js"
        }))
        .pipe(uglify({
            //outSourceMap: true,
            //report: 'min',
            compress: {
                drop_console: true
            },
            mangle: true
        }))
        .pipe(sourcemaps.write({addComment: false}))
        //.on('error', function (err) { gutil.log(gutil.colors.red('[Error]'), err.toString()); })
        .pipe(gulp.dest(_destination + '/component'));
});

gulp.task(tasks.jsMinifyShared.name, function(){
    gulp.src([
        tasks.jsMinifyShared.path
    ])
        .pipe(errorHandler())
        .pipe(ngAnnotate())
        .pipe(sourcemaps.init({ includeContent: false }))
        .pipe(rename({
            extname: ".min.js"
        }))
        .pipe(uglify({
            //outSourceMap: true,
            //report: 'min',
            compress: {
                drop_console: true
            },
            mangle: true
        }))
        .pipe(sourcemaps.write({addComment: false}))
        .pipe(gulp.dest(_destination + '/shared'));
});

//gulp.task('minify-js', [ 'transfer-js-lib', 'js-minify-app', 'js-minify-ctrl', 'js-minify-sheared' ] );
gulp.task('minify-js', [ tasks.transferJsLibrary.name, tasks.appJs.name, tasks.jsMinifyCtrl.name, tasks.jsMinifyShared.name ] );

gulp.task(tasks.imageTransferShared.name, function(){
    gulp.src(tasks.imageTransferShared.path)
        .pipe(gulp.dest(_destination + '/shared/Image'));
});

gulp.task(tasks.imageTransferAsset.name, function(){
    gulp.src(tasks.imageTransferAsset.path)
        .pipe(gulp.dest(_destination + '/assets/image'));
});

gulp.task(tasks.iconTransferAsset.name, function(){
    gulp.src(tasks.iconTransferAsset.path)
        .pipe(gulp.dest(_destination + '/assets/fonts'));
});

//gulp.task('image-icon-transfer', ['image-transfer-shared', 'image-transfer-asset', 'icon-transfer-asset' ] );
gulp.task('image-icon-transfer', [tasks.imageTransferShared.name, tasks.imageTransferAsset.name, tasks.iconTransferAsset.name ] );



gulp.task(tasks.minifyCssLib.name, function(){
    gulp.src([tasks.minifyCssLib.path])
        .pipe(rename({
            extname: ".min.css"
        }))
        .pipe(minifyCSS())
        .pipe(gulp.dest(_destination + '/assets/css'));
});

gulp.task('default', [ 'html-minify', 'minify-js', tasks.minifyCssLib.name
    //, 'image-icon-transfer'
] );

gulp.task('labAccount:watch', function () {
    gulp.watch(tasks.htmlMinifyIndex.path, [tasks.htmlMinifyIndex.name]);
    gulp.watch(tasks.htmlMinifyAll.path, [tasks.htmlMinifyAll.name]);
    gulp.watch(tasks.htmlMinifyTemplate.path, [tasks.htmlMinifyTemplate.name]);
    gulp.watch(tasks.transferJsLibrary.path, [tasks.transferJsLibrary.name]);
    gulp.watch(tasks.appJs.path, [tasks.appJs.name]);
    gulp.watch(tasks.jsMinifyCtrl.path, [tasks.jsMinifyCtrl.name]);
    gulp.watch(tasks.jsMinifyShared.path, [tasks.jsMinifyShared.name]);

    gulp.watch(tasks.imageTransferShared.path, [tasks.imageTransferShared.name]);
    gulp.watch(tasks.imageTransferAsset.path, [tasks.imageTransferAsset.name]);
    gulp.watch(tasks.iconTransferAsset.path, [tasks.iconTransferAsset.name]);
    gulp.watch(tasks.minifyCssLib.path, [tasks.minifyCssLib.name]);
});
